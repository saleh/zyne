// Internal
import "libraries-collection/environment.mjs"
// Fastify
import Fastify                    from 'fastify'
import fastifyCors                from 'fastify-cors'
import fastifyEnv                 from 'fastify-env'
import fastifyHelmet              from 'fastify-helmet'
import fastifyResponseValidation  from 'fastify-response-validation'
import fastifySensible            from 'fastify-sensible'
import fastifyCookie              from 'fastify-cookie'
import fastifyJwt                 from 'fastify-jwt'
import fastifySwagger             from 'fastify-swagger'
// Decorators
import organization    from "./decorators/organization.mjs"
import text            from "./decorators/text.mjs"
import authentication  from "./decorators/authentication.mjs"
import memory          from "./decorators/memory.mjs"
import daemons         from "./decorators/daemons.mjs"
// Plugins
import transfer   from "./plugins/transfer.mjs"
import service    from "./plugins/service.mjs"
import performer  from "./plugins/performer.mjs"
import database   from "./plugins/database.mjs"
// Application
import config   from "./core/config.mjs"
import routers  from "./routes/index.mjs"


export default async () => {

  // Fastify
  const app = Fastify(config.fastifyOptions)

  // Plugings Third-Party
  await app.register(fastifyEnv, config.envOptions)
  await app.register(fastifyCors, config.corsOptions)
  await app.register(fastifyHelmet, config.helmetOptions)
  await app.register(fastifyResponseValidation, config.responseValidationSchema)
  await app.register(fastifySensible)
  await app.register(fastifyCookie, config.cookiePluginOptions)
  await app.register(fastifyJwt, config.jwtOptions())
  if (global.environment === global.environments.DEVELOPMENT)
    await app.register(fastifySwagger, config.swaggerOptions)

  app.log.info(`[GATEWAY-WEB] Starting...`)

  // Decorators
  organization(app)
  text(app)
  daemons(app)

  // Plugins
  await app.register(service)
  await app.register(transfer)
  await app.register(performer)
  await app.register(database)

  // Memory
  memory(app)

  // Authetication
  authentication(app, {
    cookieAuthenticationConfig: config.cookieAuthenticationOptions,
    cookieAuthorization:        config.cookieAuthorization
  })
  app.addHook('preParsing', app.authentication.Verify)

  // Routers
  await routers(app)

  // Hook - onClose
  app.addHook('onClose', async () => {
    app.log.info('Shutdowning...')
    app.service.close()
    await app.database.close()
    app.log.info('Shutdowned')
  })

  // Server Methods
  try {
    await app.ready()
    if (global.environment !== global.environments.TEST) {
      /* eslint-disable no-console */
      console.log('[ ROUTES ]')
      console.log(app.printRoutes())
      /* eslint-enable no-console */
    }
  } catch (err) {
    app.log.error(err)
  }

  return app
}
