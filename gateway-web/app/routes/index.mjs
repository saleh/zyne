import uri            from '../core/uri.mjs'
// Routes
import general  from './general/index.mjs'
import blog     from './blog/index.mjs'
import comment  from './comment/index.mjs'
import auth     from './authentication/index.mjs'


export default async function Routers(app) {
  await app.register(general, { prefix: uri.HOME })
  await app.register(auth,    { prefix: uri.AUTH })
  await app.register(blog,    { prefix: uri.BLOG })
  await app.register(comment, { prefix: uri.COMMENT })
}
