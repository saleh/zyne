import Memory  from 'libraries-collection/memory.mjs'


export default (app) => {

  const memory = new Memory(app.database.redis)

  app.decorate(
    'memory',
    memory
  )
}
