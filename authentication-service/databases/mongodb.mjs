import DatabaseMongoDB  from 'libraries-collection/database/mongodb.mjs'


export default class Database extends DatabaseMongoDB {

  /**
   * Connect to database
   */
  async Connect() {
    const db = await super.Connect()
    this.collection = {
      users: db.collection('users'),
      sessions: db.collection('sessions'),
      otp: db.collection('otp')
    }
  }



  // --- [ USERS ] ---------------------------------------------------------------------------------

  async CreateUser(name, email, phone) {
    const result = await this.collection.users.insertOne({
      name,
      email,
      phone,
      enabled: true
    })

    const consequence = {
      insertedCount: result.insertedCount,
      data: null
    }
    if (result.insertedCount) {
      consequence.data = {
        id: result.ops[0]._id
      }
    }

    return consequence
  }

  async FindUser(user_id, fields = []) {
    const result = await this.collection.users.findOne(this.createObjectID(user_id))

    if (!result)
      return

    if (!Array.isArray(fields) || fields.length === 0)
      return result

    const data = {}
    fields.forEach((item) => {
      data[item] = result[item]
    })
    return data
  }

  async FindUserByEmail(email) {
    const result = await this.collection.users.findOne({
      email
    })

    if (!result)
      return

    return {
      id: result._id,
      name: result.name,
      enabled: result.enabled
    }
  }

  async FindUserByPhone(phone) {
    const result = await this.collection.users.findOne({
      phone
    })

    if (!result)
      return

    return {
      id: result._id,
      name: result.name,
      enabled: result.enabled
    }
  }



  // --- [ SESSIONS ] ------------------------------------------------------------------------------

  async CreateSession(user_id, issued_at) {
    const result = await this.collection.sessions.updateOne(
      {
        user_id: this.MongoDB.ObjectID(user_id)
      },
      { $set: {
        user_id,
        created_at: issued_at,
        issued_at
      } },
      {
        upsert: true
      }
    )

    return {
      ok: result.result.ok,
      modifiedCount: result.modifiedCount,
      upsertedCount: result.upsertedCount,
      upsertedId: result.upsertedId
    }
  }

  async DeleteSessionByUserId(user_id) {
    const result = await this.collection.sessions.deleteOne({
      user_id: this.MongoDB.ObjectID(user_id)
    })

    return {
      deletedCount: result.deletedCount,
      ok: result.result.ok
    }
  }

  async FindSessionByUserId(user_id) {
    const result = await this.collection.sessions.findOne({
      user_id: this.MongoDB.ObjectID(user_id)
    })

    if (!result)
      return

    return {
      id: result._id,
      created_at: result.created_at,
      issued_at: result.issued_at
    }
  }

  async DeleteExpiredSessions(expireDate) {
    const result = await this.collection.sessions.deleteMany({
      created_at: { $lt: expireDate }
    })

    return {
      deletedCount: result.deletedCount,
      ok: result.result.ok
    }
  }

  async UpdateSessionIssueDate(session_id, issued_at) {
    const result = await this.collection.sessions.updateOne(
      this.createObjectID(session_id),
      { $set: {
        issued_at
      } }
    )

    return {
      ok: result.result.ok,
      modifiedCount: result.modifiedCount
    }
  }

}
