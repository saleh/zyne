export default {
  HOME    : '/',
  AUTH    : '/auth',
  BLOG    : '/blog',
  COMMENT : '/comment'
}
