'use strict';


class BlogItem extends HTMLElement {
 
  constructor() {
    super();
    this.attachShadow({ mode: 'open' })

    this.onClick = this.onClick.bind(this);
  }

  connectedCallback() {
    this.render()
  }

  disconnectedCallback() {
    this.shadowRoot.getElementById('container').removeEventListener('click', this.onClick)
  }

  render() {
    const templateNode = document.getElementById(this.template)
    const content = document.importNode(templateNode.content, true)
    this.shadowRoot.appendChild(content)

    this.shadowRoot.getElementById('container').addEventListener('click', this.onClick)
  }

  get template() {
    return this.getAttribute('template')
  }

  onClick() {
    const itemClickedEvent = new CustomEvent('selected', {
      detail: {
        id: this.shadowRoot.host.dataset.id
      }
    })
    this.dispatchEvent(itemClickedEvent)
  }
}


customElements.define('blog-item', BlogItem)
