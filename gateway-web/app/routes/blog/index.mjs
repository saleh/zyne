import uri       from './uri.mjs'
import schemas   from './schemas.mjs'
import Handlers  from './handlers.mjs'


export default async function blog(app) {
  const handlers = new Handlers(app)


  /**
   * GET /blog?skip&limit
   * List of blogs
   */
  app.get(
    uri.HOME,
    { schema: schemas.list },
    handlers.List
  )

  /**
   * POST /blog/
   * Create a blog
   */
  app.post(
    uri.HOME,
    { schema: schemas.create },
    handlers.Create
  )

  /**
   * GET /blog/count
   * Count of blogs
   */
  app.get(
    uri.COUNT,
    { schema: schemas.count },
    handlers.Count
  )

  /**
   * GET /blog/:id
   * Read a blog
   */
  app.get(
    uri.params.ID,
    { schema: schemas.read },
    handlers.Read
  )

  /**
   * PUT /blog/:id
   * Update a blog
   */
  app.put(
    uri.params.ID,
    { schema: schemas.update },
    handlers.Update
  )

  /**
   * DELETE /blog/:id
   * Delete a blog
   */
  app.delete(
    uri.params.ID,
    { schema: schemas.remove },
    handlers.Delete
  )

  /**
   * GET /blog/mostliked
   * Most liked comments of a blog
   */
  app.get(
    uri.MOSTLIKED,
    { schema: schemas.mostliked },
    handlers.MostLiked
  )

}
