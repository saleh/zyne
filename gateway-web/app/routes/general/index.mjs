import uri       from './uri.mjs'
import schemas   from './schemas.mjs'
import Handlers  from './handlers.mjs'


export default async function general(app) {
  const handlers = new Handlers(app)

  /**
   * GET /
   * healthcheck
   */
  app.get(
    uri.HOME,
    { schema: schemas.healthcheck },
    handlers.Healthcheck
  )

}
