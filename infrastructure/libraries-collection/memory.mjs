// Buildin Modules
import fs    from 'fs'
import path  from 'path'
// Third-Party Modules
import yaml           from 'js-yaml'
import { Formatter }  from '@saleh-rahimzadeh/universal-nodejs/common.mjs'
// Libraries Modules
import Moment  from './moment.mjs'
import ms      from 'ms'



function checkFileExists(MEMFILE) {
  if (!fs.existsSync(MEMFILE)) {
    /* eslint-disable no-console */
    console.error(`MEMORY : Memory File (${MEMFILE}) Not Found.`)
    /* eslint-enable no-console */
    process.exit(1)
  }
}


export default class Memory {

  constructor(redis) {
    const MEMFILE = path.join(
      process.env.npm_package_constitution_resources,
      'memory.yml'
    )

    checkFileExists(MEMFILE)

    this.services = yaml.safeLoad(fs.readFileSync(MEMFILE, 'utf8'))

    this.database = redis.client
    this.redis = redis
  }

  /**
   * Generate key signature
   *
   * @param {String} service - Service name
   * @param {String|Object} endpointOrData - Endpoint name or data object
   * @param {Object} data - Data object as key name
   */
  signature(service, endpointOrData, data) {
    if (data == null)
      return Formatter("{service}:{data}", service, typeof(endpointOrData) === 'object' ? JSON.stringify(endpointOrData) : endpointOrData)
    else
      return Formatter("{service}:{endpoint}:{data}", service, endpointOrData, typeof(data) === 'object' ? JSON.stringify(data) : data)
  }

  /**
   * Fetch data from memory
   *
   * @param {String} name - Service name
   * @param {String} endpoint - Endpoint name
   * @param {Object} key - Data use as key
   */
  async fetch(name, endpoint, key) {
    const target = this.services[name][endpoint]

    const result = await this.database.get(
      this.signature(target.name, key)
    )
    return result != null ? JSON.parse(result) : null
  }

  /**
   * Accumulate data to memory
   *
   * @param {String} name - Service name
   * @param {String} endpoint - Endpoint name
   * @param {Object} key - Data use as key
   * @param {Object} data - An object of data
   */
  async accumulate(name, endpoint, key, data) {
    const target = this.services[name][endpoint]

    await this.database.setex(
      this.signature(target.name, key),
      Moment.ConvertToSeconds(ms(target.time)),
      JSON.stringify(data)
    )
  }

  /**
   * Dismiss and remove data from memory
   * It can be used as data clearer by null 'key' parameter
   *
   * @param {String} name - Service name
   * @param {String} endpoint - Endpoint name
   * @param {Object} key - Data use as key
   */
  async dismiss(name, endpoint, key) {
    const target = this.services[name][endpoint]

    let items
    if (key == null) {
      items = await this.database.keys(this.signature(target.name, "*"))
      if (items.length === 0)
        return
    } else {
      items = []
      items.push(this.signature(target.name, key))
    }

    await this.database.del(...items)
  }

}
