import BaseProvider  from 'messaging-framework/provider/base.mjs'
// Internal Modules
import fs    from 'fs'
import path  from 'path'
// Library Modules
import { Contains }  from '@saleh-rahimzadeh/universal-nodejs/common.mjs'
// Third-Party Modules
import nodemailer  from 'nodemailer'
import Mustache    from 'mustache'


function loadEmailConfig() {
  const EMAILFILE = path.join(
    process.env.npm_package_constitution_resources,
    "email.json"
  )

  if (!fs.existsSync(EMAILFILE)) {
    /* eslint-disable no-console */
    console.error(`EMAIL : Email File (${EMAILFILE}) Not Found.`)
    /* eslint-enable no-console */
    process.exit(1)
  }

  return JSON.parse(fs.readFileSync(EMAILFILE, 'utf8'))
}


export default class Provider extends BaseProvider {

  constructor(app, messenger) {
    super(app, messenger)

    this.server = loadEmailConfig()

    this.transporter = nodemailer.createTransport({
      host: this.server.host,
      port: this.server.port,
      secure: false,
      auth: {
        user: this.server.account,
        pass: process.env.EMAIL_PASSWORD
      }
    })
  }

  loadTemplate(file, data) {
    const path = 'templates/'
    return {
      text: Mustache.render(fs.readFileSync(`${path}${file}.txt`, 'utf8'), data),
      html: Mustache.render(fs.readFileSync(`${path}${file}.html`, 'utf8'), data)
    }
  }

  async transport(data, subject, template) {
    const info = await this.transporter.sendMail({
      from: this.server.sender,
      to: data.to,
      subject: subject,
      ...this.loadTemplate(template, data)
    })

    this.app.log.trace('Sent: %s - %s', template, info.messageId)
  }

  send(data, fields, name, template, subject) {
    if (!Contains(data, fields)) {
      this.app.log.error(`Missing Requirements for ${name}`)
      return false
    }

    try {
      this.transport(data, subject, template)
      return true
    } catch (error) {
      this.app.log.error(error)
      return false
    }
  }


  auth_signup(data) {
    this.send(data, ['to', 'token', 'name'], "Auth.SignUp", 'auth_signup', "Sign up to Zyne")
  }

  auth_welcome(data) {
    this.send(data, ['to', 'name'], "Auth.Welcome", 'auth_welcome', "Welcome to Zyne Project")
  }

  auth_signin(data) {
    this.send(data, ['to', 'token', 'name'], "Auth.SignIn", 'auth_signin', "Sign in to Zyne")
  }

  blog_created(data) {
    const result = this.send(data, ['to', 'blog_id', 'title'], "Blog.Created", 'blog_created', "New Blog Created")
    if (result === true)
      this.messenger.journalEmailSent({ subject: "Blog.Created", blog_id: data.blog_id })
  }

}
