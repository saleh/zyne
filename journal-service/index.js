/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
 #  Created by Saleh Rahimzadeh                                                                    #
 #  Copyright (C) 2020                                                                             #
 #  https://saleh-rahimzadeh.now.sh                                                                #
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
*/

'use strict'

// Framework
import MessagingFramework     from 'messaging-framework'
// Application
import Provider               from './app/provider.mjs'
// Subscribers
import BlogDeletedSubscriber  from './subscribers/blog-deleted.mjs'
import EmailSentSubscriber    from './subscribers/email-sent.mjs'


MessagingFramework(
  Provider,
  [
    BlogDeletedSubscriber,
    EmailSentSubscriber
  ]
)
