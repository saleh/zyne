import BaseSubscriber  from 'messaging-libraries/subscriber.mjs'


export default class AuthSignUpSubscriber extends BaseSubscriber {

  onLoad() {
    super.load(
      this.app.organization.channels[global.name].authentication.signup,
      {
        durable: false,
        acknowledgeable: false
      }
    )
  }


  onMessage(msg, data) {
    this.app.log.trace(`[${msg.getSequence()}]`, msg.getSubject())
    this.provider.auth_signup(data)
  }

}
