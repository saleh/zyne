/* [ STATUS ]
  OK = 0,
  CANCELLED = 1
  UNKNOWN = 2
  INVALID_ARGUMENT = 3
  DEADLINE_EXCEEDED = 4
  NOT_FOUND = 5
  ALREADY_EXISTS = 6
  PERMISSION_DENIED = 7
  RESOURCE_EXHAUSTED = 8
  FAILED_PRECONDITION = 9
  ABORTED = 10
  OUT_OF_RANGE = 11
  UNIMPLEMENTED = 12
  INTERNAL = 13
  UNAVAILABLE = 14
  DATA_LOSS = 15
  UNAUTHENTICATED = 16
*/

/* [ Output ]
  {
    code: STATUS
    details: string
    message: string, code+STATUS+details
  }
*/

/* [ Examples ]
  {
    code: 5,
    details: "Blog Not Found",
    message: "5 NOT_FOUND: Blog Not Found"
  }
  {
    code: 13,
    details: "Internal error in database query",
    message: "13 INTERNAL: Internal error in database query"
  }
  {
    code: 13,
    details: "Can not create blog",
    message: "13 INTERNAL: Can not create blog"
  }
  {
    code: 100,
    details: "An special error",
    message: "100 undefined: An special error"
  }
*/


export default function Exception(app) {

  /**
   * Send error by channel to client
   * @param {Error|null} error Error or null object
   * @param {Function} callback Server callback
   * @param {STATUS|null} status Error code and status
   * @param {string} message String message
   * @param {Array<string>|null} places An array of message place holders
   */
  return (error, callback, status, message, ...places) => {
    if (error)
      app.log.error(error)

    if (error != null && message === undefined)
      message = error.message
    else
      message = app.text(message, ...places)

    if (error == null && message === undefined)
      message = ''

    if (status == null)
      status = error.code

    callback({
      code: status,
      message
    })
  }

}
