export default {
  SIGNUP  : '/signup',
  SIGNIN  : '/signin',
  SIGNOUT : '/signout',
  params : {
    TOKEN   : '/:token'
  }
}
