import axios       from 'axios'
import HttpStatus  from 'http-status'
import { VueCookieNext }  from 'vue-cookie-next'
import { Formatter }      from '@saleh-rahimzadeh/universal-nodejs/common.mjs'



const cookieName = "authorization"
const uri = "/auth"

const state = {
  user: null
}


const mutations = {
  setUser(state, userId) {
    state.user = userId
  },
  populateUser(state) {
    if (VueCookieNext.isCookieAvailable(cookieName))
      state.user = VueCookieNext.getCookie(cookieName)
  },
  removeUser(state) {
    state.user = null
    VueCookieNext.removeCookie(cookieName)
  }
}


const actions = {
  async SignUp({ commit }, token) {
    let response
    try {
      response = await axios.get(Formatter("{uri}/signup/{token}", uri, token))
      if (response.status === HttpStatus.CREATED)
        commit('populateUser')
        /* NOTE:
         * It's possible to set user directly
         * commit('setUser', response.data.user_id)
         */
    } catch (error) {
      console.error(error.response.data);
      throw error.response.data
    }
  },
  async SignIn({ commit }, token) {
    let response
    try {
      response = await axios.get(Formatter("{uri}/signin/{token}", uri, token))
      if (response.status === HttpStatus.OK)
        commit('populateUser')
    } catch (error) {
      console.error(error.response.data);
      throw error.response.data
    }
  },
  async SignOut({ commit }) {
    try {
      await axios.get(Formatter("{uri}/signout", uri))
    } catch (error) {
      console.error(error.response.data);
      throw error.response.data
    } finally {
      commit('removeUser')
    }
  }
}


const getters = {
  isAuthenticated: (state) => {
    return state.user != null
  }
}


export default {
  state,
  mutations,
  actions,
  getters
}
