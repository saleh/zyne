import "libraries-collection/environment.mjs"

import serviceInstance  from 'libraries-collection/service-instance.mjs'


export default Object.assign(serviceInstance, {})
