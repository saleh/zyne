export default class Handlers {

  constructor(app) {
    this.app = app

    this.Healthcheck = this.Healthcheck.bind(this)
  }

  async Healthcheck() {
    return {
      status: '✓',
      timestamp: (new Date()).toISOString()
    }
  }

}
