/*
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
 #  Created by Saleh Rahimzadeh                                                                    #
 #  Copyright (C) 2020                                                                             #
 #  https://saleh-rahimzadeh.now.sh                                                                #
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
*/

'use strict'

// Buildin Modules
import process       from 'process'
// Third-Party Modules
import log4js        from 'log4js'
import dotenv        from 'dotenv'
// Application
import app           from './app/instance.mjs'
// Libraries Modules
import Organization  from 'libraries-collection/organization.mjs'
import Text          from 'libraries-collection/text.mjs'
import Memory        from 'libraries-collection/memory.mjs'
import Daemons       from 'libraries-collection/daemons.mjs'
// Internal Modules
import Exception            from './app/exception.mjs'
import Server               from './app/server.mjs'
// Service Modules
import ServiceSupplier      from 'services-libraries/supplier.mjs'
// Messaging Modules
import MessagingSupplier    from 'messaging-libraries/supplier.mjs'
import { MessagingCloser }  from 'messaging-libraries/supplier.mjs'


/**
 * Services Framework, a gRPC framework for services
 * @param {Provider} Provider - provider class
 * @param {Database|Array<Database>|null} Database - database class
 * @param {Service|null} Service - service class
 * @param {Messenger|null} Messenger - messenger class
 */
export default async function ServicesFramework(
  Provider,
  {
    Database = null,
    Service = null,
    Messenger = null
  },
  {
    Memorize = false
  } = {}
) {

  // --- [ BOOTSTRAPING ] --------------------------------------------------------------------------

  if (!Object.prototype.hasOwnProperty.call(process.env, 'npm_package_name'))
    throw new Error("NPM_PACKAGE_NAME Is Undefined")
  Object.assign(global, { name: process.env.npm_package_name })

  let databases
  let services, client
  let messaging, messenger
  let memory
  let server
  let provider

  process.on('SIGINT', shutdown)
  process.on('SIGTERM', shutdown)
  process.on('unhandledRejection', (reason) => { throw reason })
  process.on('uncaughtException', (error) => terminate(error))

  // --- [ PLUGINS ] -------------------------------------------------------------------------------

  if (global.environment === global.environments.PRODUCTION)
    dotenv.config()
  else
    dotenv.config({ path: '.env.development' })

  app.log = log4js.getLogger()
  app.log.level = 'ALL'

  // --- [ STARTING ] ------------------------------------------------------------------------------

  app.log.info("[%s] Starting...", global.name.toUpperCase())

  app.organization = Organization()

  app.text = Text()

  app.daemons = Daemons()

  // --- [ DATABASE ] ------------------------------------------------------------------------------

  if (Database != null) {
    app.log.info('Databases :')
    if (Array.isArray(Database)) {
      databases = {}
      try {
        Database.forEach((db) => {
          switch (Object.getPrototypeOf(db).name) {
          case 'DatabaseMongoDB':
            databases.mongodb = new db(app)
            break
          case 'DatabasePostgreSQL':
            databases.postgresql = new db(app)
            break
          case 'DatabaseRedis':
            databases.redis = new db(app)
            break
          default:
            throw new Error("Database Class Not Found")
          }
        })
      } catch (error) {
        terminate(error)
        return
      }
      try {
        for (const db of Object.getOwnPropertyNames(databases))
          await databases[db].Connect()
      } catch (error) {
        terminate(error)
        return
      }
    } else {
      databases = new Database(app)
      try {
        await databases.Connect()
      } catch (error) {
        terminate(error)
        return
      }
    }
  }

  // --- [ SERVICES ] ------------------------------------------------------------------------------

  if (Service != null && Object.prototype.hasOwnProperty.call(app.organization.dependencies, global.name)) {
    services = ServiceSupplier(
      app, 
      app.organization.dependencies[global.name]
    )
    client = new Service(services.services)
  }

  // --- [ MESSAGING ] -----------------------------------------------------------------------------

  if (Messenger != null) {
    try {
      messaging = await MessagingSupplier(app)
    } catch (error) {
      terminate(error)
      return
    }
    messenger = new Messenger(app, messaging)
  }

  // --- [ MEMORY ] --------------------------------------------------------------------------------

  if (Memorize === true) {
    if (databases == null) {
      terminate(new Error("Memory requires Redis database"))
      return
    } else if (Array.isArray(Database)) {
      if ('redis' in databases !== true) {
        terminate(new Error("Memory requires Redis database"))
        return
      }
      memory = new Memory(databases.redis)
    } else {
      if (databases.name !== 'Redis') {
        terminate(new Error("Memory requires Redis database"))
        return
      }
      memory = new Memory(databases)
    }
  }

  // --- [ PROVIDER ] ------------------------------------------------------------------------------

  provider = new Provider(
    app,
    Exception(app),
    databases,
    client,
    messenger,
    memory
  )

  if (provider != null && Object.prototype.hasOwnProperty.call(Provider.prototype, 'OnReady'))
    provider.OnReady()

  // --- [ SERVER ] --------------------------------------------------------------------------------

  try {
    server = await Server(
      app,
      provider
    )
  } catch (error) {
    terminate(error)
    return
  }

  // --- [ PROCESS ] -------------------------------------------------------------------------------

  function shutdown() {
    if (!shutdown.isCalled)
      shutdown.isCalled = true
    else
      return

    app.log.info("Shutdowning...")

    if (provider != null && Object.prototype.hasOwnProperty.call(Provider.prototype, 'OnClose'))
      provider.OnClose()

    try {
      if (server != null) {
        server.tryShutdown(() => {
          app.log.info("Server Shutdowned")
          discard()
        })
      } else
        discard()
    } catch (error) {
      terminate(error)
      return
    }
  }

  async function discard() {
    if (messaging != null) {
      await MessagingCloser(messaging, app)
      messaging = null
    }

    if (services != null) {
      services.close()
      services = null
    }

    if (databases != null) {
      app.log.info("Databases :")
      if (Array.isArray(Database)) {
        for (const db of Object.getOwnPropertyNames(databases))
          await databases[db].Close()
      } else
        await databases.Close()
      databases = null
    }

    app.log.info("[%s] Exit.", global.name.toUpperCase())
    process.exit()
  }

  function terminate(error) {
    app.log.error(error)
    process.exitCode = 1
    shutdown()
  }

}
