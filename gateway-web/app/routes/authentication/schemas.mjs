import HttpStatus  from 'http-status'


const tags = ['auth']


const signup_invite = {
  summary: "SignUp Invitation",
  description: "Request signup invitation",
  tags,
  body: {
    type: 'object',
    required: ['identity', 'name'],
    properties: {
      identity: { type: 'string' },
      name: { type: 'string' }
    }
  },
  response: {
    [HttpStatus.OK]: {}
  }
}

const signup = {
  summary: "SignUp",
  description: "SignUp by invitation token",
  tags,
  params: {
    type: 'object',
    required: ['token'],
    properties: {
      token: { type: 'string', minLength: 5, maxLength: 50 }
    }
  },
  response: {
    [HttpStatus.CREATED]: {
      description: "Successful SignUp",
      type: 'object',
      required: ['user_id'],
      properties: {
        user_id: { type: 'string' }
      }
    }
  }
}

const signin_invite = {
  summary: "SignIn Invitation",
  description: "Request signin invitation",
  tags,
  body: {
    type: 'object',
    required: ['identity'],
    properties: {
      identity: { type: 'string' }
    }
  },
  response: {
    [HttpStatus.OK]: {
      description: "Successful response",
      type: 'object'
    }
  }
}

const signin = {
  summary: "SignIn",
  description: "SignIn by invitation token",
  tags,
  params: {
    type: 'object',
    required: ['token'],
    properties: {
      token: { type: 'string', minLength: 5, maxLength: 50 }
    }
  },
  response: {
    [HttpStatus.OK]: {
      description: "Successful response",
      type: 'object',
      required: ['user_id'],
      properties: {
        user_id: { type: 'string' }
      }
    }
  }
}

const signout = {
  summary: "SignOut",
  description: "SignOut",
  tags,
  response: {
    [HttpStatus.NO_CONTENT]: {
      description: "Successful response",
      type: 'object'
    }
  }
}


export default {
  signup_invite,
  signup,
  signin_invite,
  signin,
  signout
}
