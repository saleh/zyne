import BaseProvider  from 'services-framework/provider/base.mjs'


export default class Application extends BaseProvider {

  /**
   * Get count of blogs
   */
  async Count(call, callback) {
    let result
    try {
      result = await this.database.mongodb.Count()
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    this.app.log.trace("Count: %d", result)

    callback(null, {
      count: result
    })
  }


  /**
   * Fetch list of blogs with skip and limit
   */
  async List(call) {
    const { skip , limit } = call.request
    const callback = call.destroy.bind(call)

    let blogs
    try {
      blogs = await this.database.mongodb.List({ skip, limit })
    } catch {
      return this.exception(null, callback, this.status.INTERNAL)
    }

    this.memory.accumulate(
      global.name,
      'List',
      { skip , limit },
      blogs
    )

    this.app.log.trace("List (%d:%d): %d", skip, limit, blogs.length)

    blogs.forEach(item => {
      call.write(item)
    })
    call.end()
  }


  /**
   * Create a blog
   */
  async Create(call, callback) {
    const { title, content } = call.request

    if (!title)
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'title')
    if (!content)
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'content')

    let result
    try {
      result = await this.database.mongodb.Create({ title, content })
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    if (result.insertedCount === 0)
      return this.exception(null, callback, this.status.INTERNAL, 'ERROR_CREATE_BLOG')

    this.memory.dismiss(
      global.name,
      'List'
    )

    await this.messenger.emailBlogCreated({
      to: "user@yahoo.com",
      blog_id: result.data.id,
      title: title
    })

    this.app.log.trace("Create: %s", JSON.stringify(result))

    callback(null, {
      blog_id: result.data.id
    })
  }


  /**
   * Read a blog
   */
  async Read(call, callback) {
    const { id } = call.request

    if (!id)
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'id')
    if (!this.database.mongodb.isIdValid(id))
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_INVALID_ID')

    let result
    try {
      result = await this.database.mongodb.Read({ id })
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    if (!result)
      return this.exception(null, callback, this.status.NOT_FOUND, 'ERROR_BLOG_NOT_FOUND')

    this.memory.accumulate(
      global.name,
      'Read',
      { id },
      result
    )

    this.app.log.trace("Read: %s", JSON.stringify(result))

    callback(null, {
      title: result.title,
      content: result.content
    })
  }


  /**
   * Update a blog
   */
  async Update(call, callback) {
    const { id, title, content } = call.request

    if (!id)
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'id')
    if (!this.database.mongodb.isIdValid(id))
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_INVALID_ID')
    if (!title)
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'title')
    if (!content)
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'content')

    let result
    try {
      result = await this.database.mongodb.Update({ id, title, content })
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    if (result.matchedCount === 0)
      return this.exception(null, callback, this.status.NOT_FOUND, 'ERROR_BLOG_NOT_FOUND')

    this.memory.dismiss(
      global.name,
      'Read',
      { id }
    )
    this.memory.dismiss(
      global.name,
      'List'
    )

    this.app.log.trace("Update: %s", JSON.stringify(result))

    callback(null, null)
  }


  /**
   * Delete a blog
   */
  async Delete(call, callback) {
    const { id } = call.request

    if (!id)
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'id')
    if (!this.database.mongodb.isIdValid(id))
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_INVALID_ID')

    let result
    try {
      result = await this.database.mongodb.Delete({ id })
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    if (result.deletedCount === 0)
      return this.exception(null, callback, this.status.NOT_FOUND, 'ERROR_BLOG_NOT_FOUND')

    this.memory.dismiss(
      global.name,
      'Read',
      { id }
    )
    this.memory.dismiss(
      global.name,
      'List'
    )

    await this.messenger.journalBlogDeleted({
      blog_id: id
    })

    this.app.log.trace("Delete: %s", JSON.stringify(result))

    callback(null, null)
  }


  /**
   * Most Liked comment of a blog
   */
  async MostLiked(call, callback) {
    let resultComment
    try {
      resultComment = await this.database.postgresql.MostLiked()
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    if (resultComment.rowCount === 0)
      return this.exception(null, callback, this.status.NOT_FOUND, 'ERROR_COMMENT_NOT_FOUND')

    let resultBlog
    try {
      resultBlog = await this.database.mongodb.Read({ id: resultComment.data.blog_id })
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    if (!resultBlog)
      return this.exception(null, callback, this.status.NOT_FOUND, 'ERROR_BLOG_NOT_FOUND')

    this.app.log.trace("MostLiked: %s %s", JSON.stringify(resultComment), JSON.stringify(resultBlog))

    callback(null, {
      blog_id: resultComment.data.blog_id,
      blog_title: resultBlog.title,
      blog_content: resultBlog.content,
      comment_id: resultComment.data.id,
      comment_insert_date: resultComment.data.insert_date,
      comment_user_name: resultComment.data.user_name,
      comment_content: resultComment.data.content,
      comment_likes: resultComment.data.likes
    })
  }

}
