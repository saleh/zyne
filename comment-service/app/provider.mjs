import BaseProvider  from 'services-framework/provider/base.mjs'


export default class Provider extends BaseProvider {

  /**
   * Get count of comments of a blog
   */
  async Count(call, callback) {
    const { blog_id } = call.request

    if (!blog_id)
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'blog_id')

    let result
    try {
      result = await this.database.Count({ blog_id })
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    this.app.log.trace("Count: %d", result)

    callback(null, {
      count: result
    })
  }


  /**
   * Fetch list of comments with offset and limit and blog_id
   */
  async List(call) {
    const { blog_id, offset } = call.request
    const limit = call.request.limit || 'ALL'
    const callback = call.destroy.bind(call)

    if (!blog_id)
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'blog_id')

    let result
    try {
      result = await this.database.List({ blog_id, offset, limit })
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    this.app.log.trace("List (%d:%s): %d", offset, limit, result.rowCount)

    result.comments.forEach(item => {
      call.write(item)
    })
    call.end()
  }


  /**
   * Create a comment
   */
  async Create(call, callback) {
    const { blog_id, user_name, content } = call.request

    if (!blog_id)
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'blog_id')
    if (!user_name)
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'user_name')
    if (!content)
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'content')

    let result
    try {
      result = await this.database.Create({ blog_id, user_name, content })
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    if (result.rowCount === 0)
      return this.exception(null, callback, this.status.INTERNAL, 'ERROR_CREATE_COMMENT')

    this.app.log.trace("Create: %s", JSON.stringify(result))

    callback(null, {
      comment_id: result.data.id
    })
  }


  /**
   * Delete a comment
   */
  async Delete(call, callback) {
    const { id } = call.request

    if (!id)
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'id')

    let result
    try {
      result = await this.database.Delete({ id })
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    if (result.rowCount === 0)
      return this.exception(null, callback, this.status.NOT_FOUND, 'ERROR_COMMENT_NOT_FOUND')

    this.app.log.trace("Delete: %s", JSON.stringify(result))

    callback(null, null)
  }


  /**
   * Like a comment
   */
  async Like(call, callback) {
    const { id } = call.request

    if (!id)
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'id')

    let result
    try {
      result = await this.database.Like({ id })
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    if (result.rowCount === 0)
      return this.exception(null, callback, this.status.NOT_FOUND, 'ERROR_COMMENT_NOT_FOUND')

    this.app.log.trace("Like: %s", JSON.stringify(result))

    callback(null, {
      likes: result.data.likes
    })
  }


  /**
   * Most Liked a comment
   */
  async MostLiked(call, callback) {
    let resultComment
    try {
      resultComment = await this.database.MostLiked()
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    if (resultComment.rowCount === 0)
      return this.exception(null, callback, this.status.NOT_FOUND, 'ERROR_COMMENT_NOT_FOUND')

    let resultBlog
    try {
      resultBlog = await this.services.ReadBlog({ id: resultComment.data.blog_id })
    } catch (error) {
      return this.exception(error, callback)
    }

    this.app.log.trace("MostLiked: %s %s", JSON.stringify(resultComment), JSON.stringify(resultBlog))

    callback(null, {
      blog_id: resultComment.data.blog_id,
      blog_title: resultBlog.title,
      blog_content: resultBlog.content,
      comment_id: resultComment.data.id,
      comment_insert_date: resultComment.data.insert_date,
      comment_user_name: resultComment.data.user_name,
      comment_content: resultComment.data.content,
      comment_likes: resultComment.data.likes
    })
  }

}
