require('./app/environment.js')
const { app, BrowserWindow, ipcMain, dialog, Menu } = require('electron')
const path = require('path')
const url = require('url')
const { request, GraphQLClient, gql } = require('graphql-request')
const dotenv = require('dotenv')

if (global.environment === global.environments.PRODUCTION)
  dotenv.config()
else
  dotenv.config({ path: '.env.development' })

const options = require('./app/options.js')


const gqlClient = new GraphQLClient(options.gateway.url /*, { headers: {} } */)


let mainWindow
let blogWindow


function createWindow () {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'pages', 'home', 'renderer.js'),
      contextIsolation: true
    }
  })

  mainWindow.loadFile(path.join('pages', 'home', 'index.html'))

  const mainMenu = Menu.buildFromTemplate(menuTemplate)
  Menu.setApplicationMenu(mainMenu)
}

app.whenReady().then(createWindow)

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})



const menuTemplate = [
  {
    label: 'File',
    submenu: [
      {
        label: 'Exit',
        accelerator: 'CmdOrCtrl+W',
        role: 'close'
      }
    ]
  },
  {
    label: 'Action',
    submenu: [
      {
        label: 'Add',
        accelerator: 'CmdOrCtrl+N',
        click() {
          createBlogWindow()
        }
      }
    ]
  }
]

if (process.env.NODE_ENV !== 'production') {
  menuTemplate.push({
    label: 'DevTools',
    accelerator: 'Ctrl+Shift+I',
    click() {
      mainWindow.webContents.openDevTools()
    }
  })
  menuTemplate.push({
    role: 'reload'
  })
}



ipcMain.handle('blog:list', async (event) => {
  const query = gql`
    {
      blog_list {
        id
        title
      }
    }
  `

  const result = await gqlClient.request(query)
  return result.blog_list
})

ipcMain.handle('blog:read', async (event, blogId) => {
  const query = gql`
    query blogRead($id: String!) {
      blog_read(id: $id) {
        title
        content
      }
    }
  `

  const variables = {
    id: blogId
  }

  const result = await gqlClient.request(query, variables)
  return result.blog_read
})

ipcMain.handle('comment:list', async (event, blogId) => {
  const query = gql`
    query commentList($blogid: String!) {
      comment_list(request: {
        blogId: $blogid
      }) {
        id,
        insertDate,
        userName,
        content,
        likes
      }
    }
  `

  const variables = {
    blogid: blogId
  }

  const result = await gqlClient.request(query, variables)
  return result.comment_list
})

ipcMain.handle('comment:like', async (event, commentId) => {
  const query = gql`
    mutation commentLike($id: Int!) {
      comment_like(id: $id) {
        likes
      }
    }
  `

  const variables = {
    id: parseInt(commentId)
  }

  const result = await gqlClient.request(query, variables)
  return result.comment_like
})

ipcMain.handle('comment:delete', async (event, commentId) => {
  const query = gql`
    mutation commentDelete($id: Int!) {
      comment_delete(id: $id)
    }
  `

  const variables = {
    id: parseInt(commentId)
  }

  const result = await gqlClient.request(query, variables)
  return result.comment_delete
})

ipcMain.handle('comment:create', async (event, blogid, username, content) => {
  const query = gql`
    mutation commentCreate($blogid: String!, $username: String!, $content: String!) {
      comment_create(comment: {
        blogId: $blogid,
        userName: $username,
        content: $content
      }) {
        commentId
      }
    }
  `

  const variables = {
    blogid,
    username,
    content
  }

  const result = await gqlClient.request(query, variables)
  return result.comment_create
})

ipcMain.on('dialog:commentcreated', (event, id) => {
  const options = {
    type: 'info',
    title: 'Successful',
    message: `New comment added successfully.\nID: ${id}`,
    buttons: ['OK']
  }
  dialog.showMessageBox(mainWindow, options)
})


function createBlogWindow() {
  blogWindow = new BrowserWindow({
    width: 400,
    height: 400,
    webPreferences: {
      preload: path.join(__dirname, 'pages', 'blog', 'renderer.js'),
      contextIsolation: true
    },
    parent: mainWindow,
    modal: true,
    resizable: false
  })

  blogWindow.menuBarVisible = false
  blogWindow.on('close', () => { blogWindow = null })

  blogWindow.loadFile(path.join('pages', 'blog', 'index.html'))
}

ipcMain.handle('blog:create', async (event, title, content) => {
  const query = gql`
    mutation blogCreate($title: String!, $content: String!) {
      blog_create(blog: {
        title: $title,
        content: $content
      }) {
        blogId
      }
    }
  `

  const variables = {
    title,
    content
  }

  const result = await gqlClient.request(query, variables)

  mainWindow.webContents.send('event:blog:added', result.blog_create.blogId, title)

  return result.blog_create
})

ipcMain.on('blog:new', (event) => {
  createBlogWindow()
})