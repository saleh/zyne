// Buidin Modules
import fs    from 'fs'
import path  from 'path'
import util  from 'util'
// Internal Modules
import uri         from '../core/uri.mjs'
import Moment      from 'libraries-collection/moment.mjs'
import AuthConfig  from 'libraries-collection/authentication.mjs'
// Third-Party Modules
import ms          from 'ms'
import HttpStatus  from 'http-status'


const IGNORE_URL = [
  uri.HOME
]

const IGNORE_WITH_URL = [
  "/documentation"
]


export default (app, { cookieAuthenticationConfig, cookieAuthorization }) => {
  const authconfigs = AuthConfig()


  async function Grant(reply, user_id, issued_at, created_at) {
    issued_at = new Date(issued_at)
    created_at = created_at && new Date(created_at)

    const cookieExpiresDate = new Date((created_at != null ? created_at.valueOf() : issued_at.valueOf()) + ms(authconfigs.expires.session))

    // Authentication Cookie

    const cookieAuthenticationOptions = Object.assign({}, cookieAuthenticationConfig)
    cookieAuthenticationOptions.expires = cookieExpiresDate

    const authenticationSignOptions = Object.assign({}, app.jwt.options.sign)
    authenticationSignOptions.subject = user_id
    authenticationSignOptions.expiresIn = authconfigs.expires.token

    const authenticationTokenData = await reply.jwtSign(
      {
        iat: Moment.ConvertToSeconds(issued_at.valueOf())
      },
      authenticationSignOptions
    )

    reply.setCookie(
      app.jwt.cookie.cookieName,
      authenticationTokenData,
      cookieAuthenticationOptions
    )

    // Authorization Cookie

    const cookieAuthorizationOptions = Object.assign({}, cookieAuthorization.options)
    cookieAuthorizationOptions.expires = cookieExpiresDate

    const authorizationData = user_id

    reply.setCookie(
      cookieAuthorization.name,
      authorizationData,
      cookieAuthorizationOptions
    )
  }


  async function Verify(request, reply, payload) {
    // Ignore urls from verification
    if (
      IGNORE_URL.some(url => request.url === url) ||
      IGNORE_WITH_URL.some(url => request.url.startsWith(url))
    ) return

    // JWT Verify
    let verificationError
    try {
      await request.jwtVerify()
    } catch (error) {
      verificationError = error
    }

    // Don't verify for auth api
    if (request.url.startsWith(uri.AUTH))
      return

    // Check whether auth_token is valid or not
    if (verificationError != null) {
      Deny(request, reply)
      reply.unauthorized(verificationError.message)
      return
    }

    // Check auth_token expiration
    const isExpired = (Moment.ConvertToSeconds() >= request.user.exp)
    if (!isExpired)
      return

    // Perform check session
    await app.performer(
      reply,
      { CheckSession: util.promisify(app.service.services['authentication-service'].CheckSession) },
      'CheckSession',
      {
        user_id: request.user.sub,
        issued_at: request.user.iat
      },
      false,
      false,
      async (result) => {
        await Grant(reply, request.user.sub, result.issued_at, result.created_at)
      },
      async () => {
        Deny(request, reply)
      }
    )

    return
  }


  function Deny(request, reply) {
    reply.clearCookie(app.jwt.cookie.cookieName, {})
    reply.clearCookie(cookieAuthorization.name, {})
    delete request.user
    return
  }


  async function RejectIfAuthenticated(request, reply) {
    if (IsAuthenticated(request, true)) {
      Deny(request, reply)
      reply.forbidden()
    }
    return
  }


  function IsAuthenticated(request, checkCookie = false) {
    const result = ('user' in request && request.user != null)
    if (checkCookie === true)
      return (result || app.jwt.cookie.cookieName in request.cookies)
    return result
  }


  app.decorate('authentication', {
    Grant,
    Verify,
    Deny,
    RejectIfAuthenticated,
    IsAuthenticated
  })
}
