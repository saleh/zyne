export default {
  log:          null,
  text:         null,
  organization: null,
  service:      null,
  exception:    null,
  performer:    null,
}
