import Publisher  from 'messaging-libraries/publisher.mjs'


export default class BaseMessenger {
  constructor(app, messaging) {
    this.app = app
    this.channels = app.organization.channels
    this.publish = Publisher(messaging)
  }
}
