CREATE DATABASE zyne;
-- GRANT all privileges ON DATABASE zyne;

DROP TABLE IF EXISTS comments;

CREATE TABLE comments (
	id SERIAL PRIMARY KEY,
	blog_id CHAR(24),
	insert_date TIMESTAMP default now(),
	user_name VARCHAR(50),
	content TEXT,
	likes INTEGER DEFAULT 0
);

-- id uuid DEFAULT gen_random_uuid (),
-- PRIMARY KEY (id)

--GRANT all privileges ON comments TO postgres;


-- CREATE user admin_user with password '123test';
-- ALTER USER admin_user with superuser;
-- 
-- GRANT all privileges ON DATABASE zyne TO admin_user;