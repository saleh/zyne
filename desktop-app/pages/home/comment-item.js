'use strict';


class CommentItem extends HTMLElement {
 
  constructor() {
    super();
    this.attachShadow({ mode: 'open' })

    this.onLike = this.onLike.bind(this)
    this.onRemove = this.onRemove.bind(this)
  }

  connectedCallback() {
    this.render()
  }

  disconnectedCallback() {
    this.shadowRoot.getElementById('to-like').removeEventListener('click', this.onLike)
    this.shadowRoot.getElementById('to-remove').removeEventListener('click', this.onRemove)
  }

  static get observedAttributes() {
    return ['likes']
  }

  attributeChangedCallback(attrName, oldValue, newValue) {
    if (oldValue !== newValue)
      this.render()
  }

  render() {
    while (this.shadowRoot.firstChild)
      this.shadowRoot.removeChild(this.shadowRoot.lastChild)
    const templateNode = document.getElementById(this.template)
    const content = document.importNode(templateNode.content, true)
    this.shadowRoot.appendChild(content)

    this.shadowRoot.getElementById('insertdate').innerHTML = this.dataset.insertdate
    this.shadowRoot.getElementById('username').innerHTML = this.dataset.username
    this.shadowRoot.getElementById('likes').innerHTML = this.likes

    this.shadowRoot.getElementById('to-like').addEventListener('click', this.onLike)
    this.shadowRoot.getElementById('to-remove').addEventListener('click', this.onRemove)
  }

  get template() {
    return this.getAttribute('template')
  }

  get likes() {
    return this.getAttribute('likes')
  }

  onLike() {
    const likeClickedEvent = new CustomEvent('liked', {
      detail: {
        id: this.dataset.id
      }
    })
    this.dispatchEvent(likeClickedEvent)
  }

  onRemove() {
    const removeClickedEvent = new CustomEvent('removed', {
      detail: {
        id: this.dataset.id
      }
    })
    this.dispatchEvent(removeClickedEvent)
  }
}


customElements.define('comment-item', CommentItem)
