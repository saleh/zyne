import ms  from 'ms'


export default class Moment {

  static IsExpired(issued_at, expiresExpression) {
    const issueDate = Moment.ConvertToSeconds(issued_at)
    const duration = Moment.ConvertToSeconds(ms(expiresExpression))

    return (Moment.ConvertToSeconds() - issueDate > duration)
  }

  static ConvertToSeconds(ts) {
    return Math.floor((ts || Date.now()) / 1000)
  }

}
