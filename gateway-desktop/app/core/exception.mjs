// Third-Party Modules
import { ApolloError, UserInputError, AuthenticationError, ForbiddenError }  from 'apollo-server'
import { Contains }     from '@saleh-rahimzadeh/universal-nodejs/common.mjs'


export default function exception(app) {
  return function exception(error) {
    if (error instanceof Error !== true) {
      app.log.error(error)
      throw new ApolloError(error, "ERROR")
    }

    if (Contains(error, ['code', 'details'])) {
      switch (error.code) {
      case 5:   // NOT_FOUND
        throw new ApolloError(error.details, "NOT_FOUND")
      case 3:   // INVALID_ARGUMENT
        throw new UserInputError(error.details)
      case 4:   // DEADLINE_EXCEEDED
        throw new ApolloError(error.details, "DEADLINE_EXCEEDED")
      case 6:   // ALREADY_EXISTS
        throw new ApolloError(error.details, "ALREADY_EXISTS")
      case 7:   // PERMISSION_DENIED
        throw new ForbiddenError(error.details)
      case 16:  // UNAUTHENTICATED
        throw new AuthenticationError(error.details)
      case 13:  // INTERNAL
        throw new ApolloError(error.details, "INTERNAL")
      default:  // Other
        throw new ApolloError(error.details, "ERROR")
      }
    }

    app.log.error(error)
    throw new ApolloError(error.message, "ERROR")
  }
}