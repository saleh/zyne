import BaseProvider  from 'messaging-framework/provider/base.mjs'
// Buildin Modules
import { appendFileSync }  from 'fs'
import { format }          from 'util'


const JOURNAL_FILE = '/tmp/zyne_journal'


export default class Provider extends BaseProvider {

  write(msg, data) {
    try {
      appendFileSync(
        JOURNAL_FILE,
        format("[%s] %s - %s\n", (new Date()).toLocaleString(), msg, data),
        'utf8')
    } catch (error) {
      this.app.log.error(error)
    }

    this.app.log.trace("%s: %s", msg, data)
  }


  blog_deleted(data) {
    this.write("Blog Deleted", data.blog_id)
  }

  email_sent(data) {
    this.write("Email Sent", format("Subject: %s, BlogID: %s", data.subject, data.blog_id))
  }

}
