import grpc  from '@grpc/grpc-js'


export default class BaseProvider {

  constructor(app, exception, database, services, messenger, memory) {
    this.app = app
    this.exception = exception
    this.database = database
    this.services = services
    this.messenger = messenger
    this.memory = memory

    this.status = grpc.status
  }

  /**
   * Health Checking
   */
  HealthCheck(call, callback) {
    this.app.log.info("Health Check")

    callback(null, {
      status: 1,
      timestamp: (new Date()).toISOString()
    })
  }

}
