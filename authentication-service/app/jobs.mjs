// Buildin Modules
import fs    from 'fs'
import path  from 'path'
// Internal Modules
import OTP       from '../controllers/otp.mjs'
import Sessions  from '../controllers/sessions.mjs'
// Third-Party Modules
import { CronJob }  from 'cron'


const jobs = new Map()

const discard = (function checkFileExists() {
  const DFILE = path.join(
    process.env.npm_package_constitution_resources,
    "authentication_discard.json"
  )

  if (!fs.existsSync(DFILE)) {
    /* eslint-disable no-console */
    console.error(`JOBS : Discard File (${DFILE}) Not Found.`)
    /* eslint-enable no-console */
    process.exit(1)
  }

  return JSON.parse(fs.readFileSync(DFILE, 'utf8'))
})()


function wipeSessions() {
  this.app.log.info("Wipe Sessions")
  Sessions.DeleteExpired(discard.expires.session)
}

function wipeOTP() {
  this.app.log.info("Wipe OTP")
  OTP.DeleteUnExpired()
}


export default {
  Run() {
    jobs.set('wipe_sessions', new CronJob(discard.schedules.sessions, wipeSessions.bind(this)))
    jobs.set('wipe_otp', new CronJob(discard.schedules.otp, wipeOTP.bind(this)))

    jobs.forEach((item) => {
      item.start()
    })
  },
  Stop() {
    jobs.forEach((item) => {
      item.stop()
    })
  }
}
