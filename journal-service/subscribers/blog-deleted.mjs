import BaseSubscriber  from 'messaging-libraries/subscriber.mjs'


export default class BlogDeletedSubscriber extends BaseSubscriber {

  onLoad() {
    super.load(
      this.app.organization.channels[global.name].blog.deleted,
      {
        durable: true,
        acknowledgeable: true
      }
    )
  }


  onMessage(msg, data) {
    this.app.log.trace(`[${msg.getSequence()}]`, msg.getSubject())
    this.provider.blog_deleted(data)
    msg.ack()
  }

}
