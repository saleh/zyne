/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*
 #  Created by Saleh Rahimzadeh                                                                    #
 #  Copyright (C) 2020                                                                             #
 #  https://saleh-rahimzadeh.now.sh                                                                #
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
*/

'use strict'

// Framework
import ServicesFramework  from 'services-framework'
// Application
import Provider           from './app/provider.mjs'
import Database           from './app/database.mjs'
import Service            from './app/service.mjs'


ServicesFramework(
  Provider,
  {
    Database,
    Service
  }
)
