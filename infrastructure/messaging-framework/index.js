/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
 #  Created by Saleh Rahimzadeh                                                                    #
 #  Copyright (C) 2020                                                                             #
 #  https://saleh-rahimzadeh.now.sh                                                                #
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
*/

'use strict'

// Buildin Modules
import process       from 'process'
// Third-Party Modules
import log4js        from 'log4js'
import dotenv        from 'dotenv'
// Internal Modules
import app           from './app/instance.mjs'
// Project Modules
import Organization  from 'libraries-collection/organization.mjs'
import Text          from 'libraries-collection/text.mjs'
import Daemons       from 'libraries-collection/daemons.mjs'
// Application Modules
import MessagingSupplier    from 'messaging-libraries/supplier.mjs'
import { MessagingCloser }  from 'messaging-libraries/supplier.mjs'


/**
 * Messaging Framework, a NATS Streaming framework for services
 */
export default async function MessagingFramework(Provider, Subscribers, Messenger) {

  // --- [ BOOTSTRAPING ] --------------------------------------------------------------------------

  if (!Object.prototype.hasOwnProperty.call(process.env, 'npm_package_name'))
    throw new Error("NPM_PACKAGE_NAME Is Undefined")
  Object.assign(global, { name: process.env.npm_package_name })

  let messaging, messenger
  let provider
  const subscribers = []

  process.on('SIGINT', shutdown)
  process.on('SIGTERM', shutdown)
  process.on('unhandledRejection', (reason) => { throw reason })
  process.on('uncaughtException', (error) => terminate(error))

  // --- [ PLUGINS ] -------------------------------------------------------------------------------

  if (global.environment === global.environments.PRODUCTION)
    dotenv.config()
  else
    dotenv.config({ path: '.env.development' })

  app.log = log4js.getLogger()
  app.log.level = 'ALL'

  // --- [ STARTING ] ------------------------------------------------------------------------------

  app.log.info("[%s] Starting...", global.name.toUpperCase())

  app.organization = Organization()

  app.text = Text()

  app.daemons = Daemons()

  // --- [ MESSAGING ] -----------------------------------------------------------------------------

  try {
    messaging = await MessagingSupplier(app)
  } catch (error) {
    terminate(error)
  }

  // --- [ MESSENGER ] -----------------------------------------------------------------------------

  if (Messenger != null)
    messenger = new Messenger(app, messaging)

  // --- [ PROVIDER ] ------------------------------------------------------------------------------

  provider = new Provider(
    app,
    messenger
  )

  if (provider != null && Object.prototype.hasOwnProperty.call(Provider.prototype, 'OnReady'))
    provider.OnReady()

  // --- [ SUBSCRIBERS ] ---------------------------------------------------------------------------

  app.log.info("Subscribers :")

  for (const subscriber of Subscribers) {
    const subItem = new subscriber(app, messaging, provider)
    subItem.onLoad()
    await subItem.subscribe()
    subscribers.push(subItem)
  }

  app.log.info("Ready")

  // --- [ PROCESS ] -------------------------------------------------------------------------------

  function shutdown() {
    if (!shutdown.isCalled)
      shutdown.isCalled = true
    else
      return

    app.log.info("Shutdowning...")

    try {
      discard()
    } catch (error) {
      terminate(error)
    }
  }

  async function discard() {
    if (provider != null && Object.prototype.hasOwnProperty.call(Provider.prototype, 'OnClose'))
      await provider.OnClose()

    if (subscribers.length > 0) {
      app.log.info("Subscribers :")
      for (const subItem of subscribers)
        await subItem.unsubscribe()
    }

    if (messaging != null) {
      await MessagingCloser(messaging, app)
      messaging = null
    }

    app.log.info("[%s] Exit.", global.name.toUpperCase())
    process.exit()
  }

  function terminate(error) {
    app.log.error(error)
    process.exitCode = 1
    shutdown()
  }

}
