import DatabaseMongoDB  from 'libraries-collection/database/mongodb.mjs'


export default class Database extends DatabaseMongoDB {

  /**
   * Connect to database
   */
  async Connect() {
    const db = await super.Connect()
    this.collection = db.collection('blogs')
  }


  async Count() {
    return await this.collection.countDocuments({})
  }


  async List({ skip, limit }) {
    const cursor = this.collection.find().skip(skip).limit(limit)
    const blogs = []

    for await(const blog of cursor) {
      blogs.push({
        id: blog._id,
        title: blog.title
      })
    }

    return blogs
  }


  async Create({ title, content }) {
    const result = await this.collection.insertOne({
      'title': title,
      'content': content
    })

    const consequence = {
      insertedCount: result.insertedCount,
      data: null
    }
    if (result.insertedCount) {
      consequence.data = {
        id: result.ops[0]._id
      }
    }

    return consequence
  }


  async Read({ id }) {
    const result = await this.collection.findOne(this.createObjectID(id))

    if (!result)
      return

    return {
      title: result.title,
      content: result.content
    }
  }


  async Update({ id, title, content }) {
    const result = await this.collection.updateOne(
      this.createObjectID(id),
      { $set: {
        'title': title,
        'content': content
      } }
    )

    return {
      matchedCount: result.matchedCount,
      modified: result.result.nModified,
      ok: result.result.ok
    }
  }


  async Delete({ id }) {
    const result = await this.collection.deleteOne(this.createObjectID(id))

    return {
      deletedCount: result.deletedCount,
      ok: result.result.ok
    }
  }

}
