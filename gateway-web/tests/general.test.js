'use strict'

var global = {}

// Application
import uri          from '../app/core/uri.js'
import application  from '../app/application.js'


let app

beforeAll(async () => {
  app = await application()
})

afterAll(async () => {
  app.close()
})


test("Healthcheck", async () => {
  const response = await app.inject({
    method: 'GET',
    url: uri.API
  })
  expect(response).not.toBeNull()
  expect(response.statusCode).toEqual(200)
  const result = JSON.parse(response.body)
  expect(result).toEqual(expect.objectContaining({
    status: expect.any(String),
    timestamp: expect.any(String)
  }))
  expect(result.status).toMatch(/✓/)
})
