#!/bin/bash

# Volumes

docker volume inspect --format '{{ .Name }}' zyne_mongodb_volume
if [ $? = 1 ]
then
docker volume create zyne_mongodb_volume
fi

docker volume inspect --format '{{ .Name }}' zyne_postgres_volume
if [ $? = 1 ]
then
docker volume create zyne_postgres_volume
fi

docker volume inspect --format '{{ .Name }}' zyne_redis_volume
if [ $? = 1 ]
then
docker volume create zyne_redis_volume
fi

# Networks

docker network inspect --format '{{ .Name }}' zyne_network
if [ $? = 1 ]
then
docker network create --driver bridge zyne_network
fi
