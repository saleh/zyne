import general  from './resolvers/general.mjs'
import Blog     from './resolvers/blog.mjs'
import Comment  from './resolvers/comment.mjs'


let blogs, comments


export default function resolvers(app) {
  blogs = new Blog(app)
  comments = new Comment(app)
}


export function resolverDefs() {
  const Query = {
    healthcheck: general.healthcheck,
    blog_count: blogs.Count,
    blog_list: blogs.List,
    blog_read: blogs.Read,
    blog_mostliked: blogs.MostLiked,
    comment_count: comments.Count,
    comment_list: comments.List,
    comment_mostliked: comments.MostLiked
  }

  const Mutation = {
    blog_create: blogs.Create,
    blog_update: blogs.Update,
    blog_delete: blogs.Delete,
    comment_create: comments.Create,
    comment_delete: comments.Delete,
    comment_like: comments.Like
  }

  return {
    Query,
    Mutation
  }
}