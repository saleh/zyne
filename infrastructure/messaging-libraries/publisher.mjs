function encodeMessage(data) {
  return JSON.stringify(data)
}


export default function Publisher(messaging) {

  return function Publish(channel, data) {
    return new Promise((resolve, reject) => {
      messaging.publish(channel, encodeMessage(data), (error, guid) => {
        if (error)
          reject(error)

        resolve(guid)
      })
    })
  }

}
