// Library Modules
import Moment               from 'libraries-collection/moment.mjs'
import { ExpressionTypes }  from 'libraries-collection/regexes.mjs'
// Third-Party Modules
import ms  from 'ms'
import { Formatter }  from '@saleh-rahimzadeh/universal-nodejs/common.mjs'


const SIGNATURE = {
  KEY: "OTP",
  SIGNUP : "SIGNUP",
  SIGNIN : "SIGNIN"
}

/**
 * Generate REDIS key
 *
 * @param {String} type - OTP type
 * @param {String} token - OTP token
 */
function key(type = "", token = "") {
  return Formatter("{key}:{type}:{token}", SIGNATURE.KEY, type, token)
}


export default class OTP {

  static async Create(callback, signature, token, identity, data) {
    const otpKey = key(signature, token)

    const result = await this.database.redis.CreateOtp(
      otpKey,
      Moment.ConvertToSeconds(ms(this.authconfigs.expires.otp)),
      identity,
      data
    )

    if (!result)
      return this.exception(null, callback, this.status.INTERNAL, 'ERROR_CREATE_OTP')

    return otpKey
  }

  static async CreateSignUp(callback, token, identity, name) {
    return await OTP.Create(callback, SIGNATURE.SIGNUP, token, identity, { name })
  }

  static async CreateSignIn(callback, token, identity) {
    return await OTP.Create(callback, SIGNATURE.SIGNIN, token, identity)
  }


  static async Search(callback, signature, token, ...fields) {
    const otpKey = key(signature, token)

    if (await this.database.redis.IsOtpExists(otpKey) === false)
      return this.exception(null, callback, this.status.NOT_FOUND, 'ERROR_OTP_NOT_FOUND')

    const result = await this.database.redis.FindOtp(otpKey, fields)

    return result
  }

  static async SearchSignUp(callback, token) {
    const result = await OTP.Search(callback, SIGNATURE.SIGNUP, token, "name")
    return result && {
      identity: result.identity,
      name: result.data[0]
    }
  }

  static async SearchSignIn(callback, token) {
    const result = await OTP.Search(callback, SIGNATURE.SIGNIN, token)
    return result && {
      identity: result.identity
    }
  }


  static async RemoveSignUp(token) {
    await this.database.redis.DeleteOtp(key(SIGNATURE.SIGNUP, token))
  }

  static async RemoveSignIn(token) {
    await this.database.redis.DeleteOtp(key(SIGNATURE.SIGNIN, token))
  }


  static async SendSignUp(callback, identityType, identity, token, name) {
    switch (identityType) {
    case ExpressionTypes.email:
      if (await this.messenger.emailSignUpInvite({ to: identity, token, name }) !== true)
        return this.exception(null, callback, this.status.INTERNAL, 'ERROR_SIGNUP_SEND_EMAIL')
      break
    case ExpressionTypes.phone:
      // TODO: Implement Phone SignIn invite
      break
    }
  }

  static async SendSignIn(callback, identityType, identity, token, name) {
    switch (identityType) {
    case ExpressionTypes.email:
      if (await this.messenger.emailSignInInvite({ to: identity, token, name }) !== true)
        return this.exception(null, callback, this.status.INTERNAL, 'ERROR_SIGNIN_SEND_EMAIL')
      break
    case ExpressionTypes.phone:
      // TODO: Implement Phone SignIn invite
      break
    }
  }


  static async DeleteUnExpired() {
    let items = await this.database.redis.GetAllOtp(key("*", "*"))
    if (items.length === 0)
      return

    items = items.filter(async (item) => {
      await this.database.redis.GetOtpTTL(item) === -1
    })

    if (items.length > 0)
      this.database.redis.RemoveOtp(items)
  }

}
