import uri       from './uri.mjs'
import schemas   from './schemas.mjs'
import Handlers  from './handlers.mjs'


export default async function blog(app) {
  const handlers = new Handlers(app)


  /**
   * GET /comment?blog_id&offset&limit
   * List of comments
   */
  app.get(
    uri.HOME,
    { schema: schemas.list },
    handlers.List
  )

  /**
   * POST /comment/
   * Create a comment
   */
  app.post(
    uri.HOME,
    { schema: schemas.create },
    handlers.Create
  )

  /**
   * GET /comment/count?blog_id
   * Count of comments
   */
  app.get(
    uri.COUNT,
    { schema: schemas.count },
    handlers.Count
  )

  /**
   * PATCH /comment/:id/like
   * Like a comment
   */
  app.patch(
    `${uri.params.ID}${uri.LIKE}`,
    { schema: schemas.like },
    handlers.Like
  )

  /**
   * DELETE /comment/:id
   * Delete a comment
   */
  app.delete(
    uri.params.ID,
    { schema: schemas.remove },
    handlers.Delete
  )

  /**
   * GET /comment/mostliked
   * Most liked comments of a blog
   */
  app.get(
    uri.MOSTLIKED,
    { schema: schemas.mostliked },
    handlers.MostLiked
  )

}
