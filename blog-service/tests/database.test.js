import app  from './instance.js'
import Database  from '../app/database-mongodb.js'

const database = new Database(app)


beforeAll(async () => {
  await database.Connect()
  await database.Collection.deleteMany({})
})

afterAll(async () => {
  await database.Collection.drop()
  await database.Close()
})


test("Insert a blog", async () => {
  const result = await database.Create({ title: 'blog1', content: 'Blog 10' })
  expect(result).not.toBeNull()
  expect(result.insertedCount).toBe(1)
  expect(result.data).toEqual(expect.objectContaining({
    id: expect.any(Object)
  }))
  expect(database.isIdValid(result.data.id)).toBeTruthy()
})

test("Count blogs to be 1", async () => {
  const result = await database.Count()
  expect(result).toBe(1)
})

test("List blogs to be 1", async () => {
  const result = await database.List({ skip: 0, limit: 0 })
  expect(result.length).toBe(1)
})

test("Read a blog", async () => {
  const list = await database.List({ skip: 0, limit: 0 })
  const result = await database.Read(list[0].id)
  expect(result).not.toBeNull()
  expect(result).toEqual(expect.objectContaining({
    title: expect.any(String),
    content: expect.any(String)
  }))
  expect(result.title).toMatch(/blog1/)
  expect(result.content).toMatch(/Blog/)
})

test("Update a blog", async () => {
  const list = await database.List({ skip: 0, limit: 0 })
  const result = await database.Update(list[0].id, 'blog11', 'Blog 11')
  expect(result.matchedCount).toEqual(1)
  expect(result.modified).toEqual(1)
})

test("Delete a blog", async () => {
  const list = await database.List({ skip: 0, limit: 0 })
  const result = await database.Delete(list[0].id)
  expect(result.deletedCount).toEqual(1)
})
