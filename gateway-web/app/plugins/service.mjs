// Third-Party
import fastifyPlugin    from 'fastify-plugin'
// Library
import ServiceSupplier  from 'services-libraries/supplier.mjs'


export default fastifyPlugin(async (app) => {
  app.decorate('service',
    ServiceSupplier({
      log: app.log,
      organization: app.organization
    })
  )
})
