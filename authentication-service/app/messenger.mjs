import BaseMessenger         from 'services-framework/messenger/base.mjs'
import ServicesNameResolver  from 'libraries-collection/services-name-resolver.mjs'


export default class Messenger extends BaseMessenger {

  async emailSignUpInvite(data) {
    try {
      await this.publish(this.channels[ServicesNameResolver.EMAIL_SERVICE].authentication.signup, data)
      return true
    } catch (error) {
      return false
    }
  }

  async emailSignUpWelcome(data) {
    try {
      await this.publish(this.channels[ServicesNameResolver.EMAIL_SERVICE].authentication.welcome, data)
      return true
    } catch (error) {
      return false
    }
  }

  async emailSignInInvite(data) {
    try {
      await this.publish(this.channels[ServicesNameResolver.EMAIL_SERVICE].authentication.signin, data)
      return true
    } catch (error) {
      return false
    }
  }

}
