export default {
  HOME      : '/',
  COUNT     : '/count',
  LIKE      : '/like',
  MOSTLIKED : '/mostliked',
  params : {
    ID : '/:id'
  }
}
