import DatabasePostgreSQL  from 'libraries-collection/database/postgresql.mjs'
import pgFormat            from 'pg-format'


export default class Database extends DatabasePostgreSQL {

  async Count({ blog_id }) {
    const result = await this.client.query({
      text: "SELECT CAST(COUNT(*) AS integer) FROM comments WHERE blog_id=$1",
      values: [blog_id]
    })
    return result.rows[0].count
  }


  async List({ blog_id, offset, limit }) {
    const result = await this.client.query({
      text: pgFormat('SELECT id, insert_date, user_name, content, likes FROM comments WHERE blog_id=$1 ORDER BY insert_date LIMIT %s OFFSET $2', limit),
      values: [blog_id, offset]
    })

    return {
      rowCount: result.rowCount,
      comments: result.rows
    }
  }


  async Create({ blog_id, user_name, content }) {
    const result = await this.client.query({
      text: 'INSERT INTO comments (blog_id, insert_date, user_name, content) VALUES ($1, now(), $2, $3) RETURNING *',
      values: [blog_id, user_name, content]
    })

    const consequence = {
      rowCount: result.rowCount,
      data: null
    }
    if (result.rowCount) {
      consequence.data = {
        id: result.rows[0].id
      }
    }

    return consequence
  }


  async Delete({ id }) {
    const result = await this.client.query({
      text: 'DELETE FROM comments WHERE id=$1',
      values: [id]
    })

    return {
      rowCount: result.rowCount
    }
  }


  async Like({ id }) {
    const result = await this.client.query({
      text: 'UPDATE comments SET likes = likes+1 WHERE id=$1 RETURNING likes',
      values: [id]
    })

    const consequence = {
      rowCount: result.rowCount,
      data: null
    }
    if (result.rowCount) {
      consequence.data = {
        likes: result.rows[0].likes
      }
    }

    return consequence
  }


  async MostLiked() {
    const result = await this.client.query({
      text: "SELECT * FROM comments WHERE likes = (SELECT MAX(likes) AS max FROM comments)"
    })

    const consequence = {
      rowCount: result.rowCount,
      data: null
    }
    if (result.rowCount)
      consequence.data = result.rows[0]

    return consequence
  }

}
