// Internal Modules
import Resolver  from '../core/resolver.mjs'
// Library Modules
import ServicesNameResolver  from 'libraries-collection/services-name-resolver.mjs'


export default class Blog extends Resolver {

  constructor(app) {
    super(app, ServicesNameResolver.COMMENT_SERVICE, 
      [
        'Create',
        'Count',
        'Delete',
        'Like',
        'MostLiked'
      ],
      [
        'List'
      ]
    )
  }


  async List(parent, args, context, info) {
    const { blogId, offset = 0, limit = 0 } = args.request
    return await context.performer(this.service, 'List', { blog_id: blogId, offset, limit }, (result) => { 
      return result.map(item => ({
        id: item.id, 
        insertDate: item.insert_date,
        userName: item.user_name,
        content: item.content,
        likes: item.likes
      }))
    })
  }
  
  async Count(parent, args, context, info) {
    const blogId = args.blogId
    return await context.performer(this.service, 'Count', { blog_id: blogId }, (result) => result.count)
  }

  async Create(parent, args, context, info) {
    const { blogId, userName, content } = args.comment
    return await context.performer(this.service, 'Create', { blog_id: blogId, user_name: userName, content }, (result) => ({ commentId: result.comment_id }))
  }

  async MostLiked(parent, args, context, info) {
    return await context.performer(this.service, 'MostLiked', null, (result) => ({
      blogId: result.blog_id,
      blogTitle: result.blog_title,
      blogContent: result.blog_content,
      commentId: result.comment_id,
      commentInsertDate: result.comment_insert_date,
      commentUserName: result.comment_user_name,
      commentContent: result.comment_content,
      commentLikes: result.comment_likes
    }))
  }

  async Delete(parent, args, context, info) {
    const id = args.id
    return await context.performer(this.service, 'Delete', { id }, () => true)
  }

  async Like(parent, args, context, info) {
    const id = args.id
    return await context.performer(this.service, 'Like', { id })
  }

}