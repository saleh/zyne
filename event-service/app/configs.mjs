// Library Modules
import AuthConfig, { SecretKeyPublic }  from 'libraries-collection/authentication.mjs'


const jwtOptions = {
  secret: SecretKeyPublic(),
  ...AuthConfig().jsonwebtoken
}

const EVENT_HEADERS = {
  'Content-Type'  : "text/event-stream",
  'Cache-Control' : "no-cache,no-transform",
  'Connection'    : "keep-alive"
}

const RESPONSE_CORS_HEADERS = {
  Origin      : 'Access-Control-Allow-Origin',
  Credentials : 'Access-Control-Allow-Credentials',
}

const RESPONSE_HEADERS = {
  'Access-Control-Allow-Methods' : "GET",
  'Access-Control-Allow-Headers' : "Origin, X-Requested-With, Content-Type, Accept"
}


export default {
  jwtOptions,
  EVENT_HEADERS,
  RESPONSE_CORS_HEADERS,
  RESPONSE_HEADERS
}
