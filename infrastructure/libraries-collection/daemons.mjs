// Buildin Modules
import fs    from 'fs'
import path  from 'path'
// Third-Party Modules
import yaml  from 'js-yaml'


const DAEMONFILE = path.join(
  process.env.npm_package_constitution_resources,
  'daemons.yml'
)


function checkFileExists() {
  if (!fs.existsSync(DAEMONFILE)) {
    /* eslint-disable no-console */
    console.error(`DAEMONS : Daemon File (${DAEMONFILE}) Not Found.`)
    /* eslint-enable no-console */
    process.exit(1)
  }
}


export default function Daemons() {
  checkFileExists()
  return yaml.safeLoad(fs.readFileSync(DAEMONFILE, 'utf8'))[global.environment]
}
