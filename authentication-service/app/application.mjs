// Libraries Modules
import RegExChecker, { ExpressionTypes }  from 'libraries-collection/regexes.mjs'
import BaseProvider    from 'services-framework/provider/base.mjs'
import Moment          from 'libraries-collection/moment.mjs'
import AuthConfig      from 'libraries-collection/authentication.mjs'
// Third-Party Modules
import { Binder }  from '@saleh-rahimzadeh/universal-nodejs/common.mjs'
// Internal Modules
import Jobs            from './jobs.mjs'
import * as Tokenizer  from './tokenizer.mjs'
// Controller Modules
import OTP       from '../controllers/otp.mjs'
import Users     from '../controllers/users.mjs'
import Sessions  from '../controllers/sessions.mjs'


export default class Application extends BaseProvider {

  OnReady() {
    this.authconfigs = AuthConfig()

    Binder(OTP, this)
    Binder(Users, this)
    Binder(Sessions, this)

    Jobs.Run.call(this)
  }

  OnClose() {
    Jobs.Stop.call(this)
  }



  async SignUpInvite(call, callback) {
    const { identity, name } = call.request

    if (!identity) return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'identity')
    if (!name) return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'name')

    // Inference identity type
    let identityType
    try {
      identityType = RegExChecker.whatIsThis(identity)
    } catch (error) {
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_INVALID_IDENTITY')
    }

    // Check user not exists
    if (await Users.CheckNotExists(callback, identityType, identity) === true) return

    // Generate token password
    const token = Tokenizer.SignUpTokenGenerate(identityType)

    // Create OTP to memory
    if (await OTP.CreateSignUp(callback, token, identity, name) == null) return

    // Send OTP
    OTP.SendSignUp(callback, identityType, identity, token, name)

    this.app.log.trace("SignUpInvite: %s : %s", identity, token)

    callback(null, null)
  }



  async SignUp(call, callback) {
    const { token } = call.request

    if (!token) return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'token')

    // Search OTP in memory
    const resultOTP = await OTP.SearchSignUp(callback, token)
    if (resultOTP == null) return

    // Remove OTP
    await OTP.RemoveSignUp(token)

    // Inference identity type
    let identityType = RegExChecker.whatIsThis(resultOTP.identity)

    // Fetch fields from identity type
    let email = ''
    let phone = ''
    switch (identityType) {
    case ExpressionTypes.email:
      email = resultOTP.identity
      break
    case ExpressionTypes.phone:
      phone = resultOTP.identity
      break
    }

    // Create new user to database
    const resultUser = await Users.Create(callback, resultOTP.name, email, phone)
    if (resultUser == null) return

    // Send Welcome
    switch (identityType) {
    case ExpressionTypes.email:
      if (await this.messenger.emailSignUpWelcome({ to: resultOTP.identity, name: resultOTP.name }) !== true)
        return this.exception(null, callback, this.status.INTERNAL, 'ERROR_SIGNUP_SEND_EMAIL')
      break
    case ExpressionTypes.phone:
      // TODO: Implement Phone SignUp welcome
      break
    }

    // Create new session to database
    const resultSession = await Sessions.Create(callback, resultUser.data.id)
    if (resultSession == null) return

    this.app.log.trace("SignUp: \n%s\n%s", JSON.stringify(resultUser), JSON.stringify(resultSession))

    callback(null, {
      user_id: resultUser.data.id,
      issued_at: resultSession.issued_at.toISOString()
    })
  }



  async SignInInvite(call, callback) {
    const { identity } = call.request

    if (!identity) return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'identity')

    // Inference identity type
    let identityType
    try {
      identityType = RegExChecker.whatIsThis(identity)
    } catch (error) {
      return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_INVALID_IDENTITY')
    }

    // Check user exists
    const resultUser = await Users.CheckExists(callback, identityType, identity)
    if (resultUser == null) return

    // Generate token password
    const token = Tokenizer.SignInTokenGenerate(identityType)

    // Create OTP to memory
    if (await OTP.CreateSignIn(callback, token, identity) == null) return

    // Send OTP
    OTP.SendSignIn(callback, identityType, identity, token, resultUser.name)

    this.app.log.trace("SignInInvite: %s : %s", identity, token)

    callback(null, null)
  }



  async SignIn(call, callback) {
    const { token } = call.request

    if (!token) return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'token')

    // Search OTP in memory
    const resultOTP = await OTP.SearchSignIn(callback, token)
    if (resultOTP == null) return

    // Remove OTP
    await OTP.RemoveSignIn(token)

    // Inference identity type
    const identityType = RegExChecker.whatIsThis(resultOTP.identity)

    // Check user exists and get user
    const resultUser = await Users.CheckExists(callback, identityType, resultOTP.identity)
    if (resultUser == null) return

    // Check whether user is enabled or not
    if (!resultUser.enabled) {
      await Sessions.DeleteByUserId(resultUser.id)
      return this.exception(null, callback, this.status.PERMISSION_DENIED, 'ERROR_USER_DISABLED')
    }

    // Create new session to database
    const resultSession = await Sessions.Create(callback, resultUser.id)
    if (resultSession == null) return

    this.app.log.trace("SignIn: \n%s\n%s", JSON.stringify(resultUser), JSON.stringify(resultSession))

    callback(null, {
      user_id: resultUser.id,
      issued_at: resultSession.issued_at.toISOString()
    })
  }



  async CheckSession(call, callback) {
    const { user_id, issued_at } = call.request

    if (!user_id) return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'user_id')
    if (!this.database.mongodb.isIdValid(user_id)) return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_INVALID_ID')
    if (!issued_at) return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'issued_at')

    // Find user
    const resultUser = await Users.Find(callback, user_id)
    if (resultUser == null) return

    // Check whether user is enabled or not
    if (!resultUser.enabled) {
      await Sessions.DeleteByUserId(user_id)
      return this.exception(null, callback, this.status.PERMISSION_DENIED, 'ERROR_USER_DISABLED')
    }

    // Find session
    const resultSession = await Sessions.FindByUserId(callback, user_id)
    if (resultSession == null) return

    // Check session issue date
    if (this.authconfigs.tolerant === false && Moment.ConvertToSeconds(resultSession.issued_at.valueOf()) !== parseInt(issued_at))
      return this.exception(null, callback, this.status.UNAUTHENTICATED, 'ERROR_SESSION_INVALID')

    // Check Session expiration
    if (Moment.IsExpired(resultSession.created_at, this.authconfigs.expires.session)) {
      await Sessions.DeleteByUserId(user_id)
      return this.exception(null, callback, this.status.UNAUTHENTICATED, 'ERROR_SESSION_EXPIRED')
    }

    // Update session issue date
    const resultSessionUpdate = await Sessions.UpdateIssueDate(callback, resultSession.id)
    if (resultSessionUpdate == null) return

    this.app.log.trace("CheckSession: \n%s\n%s\n%s", JSON.stringify(resultUser), JSON.stringify(resultSession), JSON.stringify(resultSessionUpdate))

    callback(null, {
      issued_at: resultSessionUpdate.issued_at.toISOString(),
      created_at: resultSession.created_at.toISOString()
    })
  }



  async SignOut(call, callback) {
    const { user_id } = call.request

    if (!user_id) return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_EMPTY_ARGUMENT', 'user_id')
    if (!this.database.mongodb.isIdValid(user_id)) return this.exception(null, callback, this.status.INVALID_ARGUMENT, 'ERROR_INVALID_ID')

    // Check user exits
    if (await Users.IsExist(callback, user_id) !== true) return

    // Delete session
    await Sessions.DeleteByUserId(user_id)

    this.app.log.trace("SignOut: %s", user_id)

    callback(null, null)
  }

}
