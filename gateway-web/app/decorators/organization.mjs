import Organization  from 'libraries-collection/organization.mjs'


export default (app) => {
  app.decorate(
    'organization',
    Organization()
  )
}
