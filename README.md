# ZYNE

A Microservice Solution And Framework, Using Cutting-Edge Technologies.


Languages:
JavaScript

Runtimes:
Node.js

RESTful Framework:
Fastify.js

Frontend Framework:
Vue.js (vue-router, vuex)
Electron.js

RPC Framework:
gRPC

GraphQL Framework:
Apollo Server
Graphql Request

Message Broker:
NATS Streaming

Databases:
MongoDB
PostgreSQL
Redis

Reverse Proxy & Web Server:
Nginx

Container:
Docker

Test:
Jest

Lint:
ESLint

HTML & CSS Framework:
Bootstrap
primevue
WebComponents
