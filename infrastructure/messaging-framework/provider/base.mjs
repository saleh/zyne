export default class BaseProvider {

  constructor(app, messenger) {
    this.app = app
    this.messenger = messenger
  }

}
