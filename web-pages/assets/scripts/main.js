const url = "http://localhost:5000"

function isAuthenticated() {
  return document.cookie
    .split(";")
    .map(item => item.trim())
    .some(item => item.startsWith("authorization"))
}
