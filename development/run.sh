#!/bin/bash

if [ $(basename $(dirname $(pwd))) = 'infrastructure' ]; then
PARENT="../.."
else
PARENT=".."
fi

case $1 in
  "watch")
    nodemon --config $PARENT/development/nodemon.json
    ;;
  "test")
    NODE_OPTIONS=--experimental-vm-modules npx jest .
    ;;
  "lint")
    eslint --config $PARENT/development/eslintrc.yml --format table --color --ignore-path $PARENT/development/.eslintignore ./**/*.mjs ./*.*js
    ;;
  *)
    ;;
esac
