import { createStore } from 'vuex'
import authentication  from './authentication.mjs'


export default createStore({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    authentication
  }
})
