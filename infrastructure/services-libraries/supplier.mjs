import ServiceDescriptor  from './descriptor.mjs'
import ServiceClient      from './client.mjs'


export default function ServiceSupplier(app, targets) {
  app.log.info("Services :")

  if (targets == null)
    targets = Object.keys(app.organization.descriptors)

  const services = {}

  for (const serve of targets) {
    app.log.info("\t\t [%s] Connecting...", serve.toUpperCase())
    services[serve] =
      ServiceClient(
        ServiceDescriptor(serve, app.organization.protobuf),
        app.organization,
        serve
      )
    app.log.info("\t\t [%s] Connected", serve.toUpperCase())
  }

  function close() {
    if (Object.keys(services).length > 0) {
      app.log.info("Services :")
      for (const item in services) {
        services[item].close()
        app.log.info("\t\t [%s] Closed", item.toUpperCase())
      }
    }
  }

  return {
    close,
    services
  }
}
