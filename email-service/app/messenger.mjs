import BaseMessenger  from 'messaging-framework/messenger/base.mjs'
import ServicesNameResolver  from 'libraries-collection/services-name-resolver.mjs'


export default class Messenger extends BaseMessenger {

  async journalEmailSent(data) {
    try {
      await this.publish(this.channels[ServicesNameResolver.JOURNAL_SERVICE].email.sent, data)
    } catch (error) {
      this.app.log.error(error)
    }
  }

}
