class EventServer {

  constructor(url) {
    this.server = null
    this.url = url
  }

  start(app) {
    if (!app.$store || !app.$store.getters.isAuthenticated)
      return

    this.server = new EventSource(this.url, { withCredentials: true })

    this.server.onopen = (e) => {
    }

    this.server.onmessage = (e) => {
    }

    this.server.onerror = (e) => {
      if (e.target.readyState == EventSource.CLOSED) {
      }
    }
  }

  close() {
    if (this.server != null) {
      this.server.close()
      this.server = null
    }
  }

  addListener(channel, callback) {
    this.server.addEventListener(channel,callback)
  }

  removeListener(channel, callback) {
    this.server.removeEventListener(channel,callback)
  }

}


export default {
  install(app, options) {
    app.config.globalProperties.$EventServer = new EventServer(options.url)
  }
}
