import BaseSubscriber  from 'messaging-libraries/subscriber.mjs'


export default class AuthWelcomeSubscriber extends BaseSubscriber {

  onLoad() {
    super.load(
      this.app.organization.channels[global.name].authentication.welcome,
      {
        durable: false,
        acknowledgeable: false
      }
    )
  }


  onMessage(msg, data) {
    this.app.log.trace(`[${msg.getSequence()}]`, msg.getSubject())
    this.provider.auth_welcome(data)
  }

}
