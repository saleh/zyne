import fastifyPlugin  from 'fastify-plugin'


export default fastifyPlugin(async (app) => {

  /**
   * Perfomer, performe operation on service
   *
   * @param {Object} data - The data to send to service
   * @param {Service} service - An promisified service to call
   * @param {Reply} reply - The Reply object of fastify
   * @param {Number|Boolean} status - Status code of http to reply, if Boolean then don't send Reply
   * @param {Function|Null} callbackSuccess - Calllback function to call after calling service successfully with result, if null then return service result
   * @param {Function|Null} callbackError - Callback function to call on calling service has exception
   */
  async function performer(reply, service, endpoint, data, status, cache, callbackSuccess, callbackError) {
    let result

    if (cache === true)
      result = await app.memory.fetch(service.name, endpoint, data)

    if (result == null) {
      try {
        result = await service[endpoint](data)
      } catch (error) {
        callbackError && await callbackError()
        app.transfer.error(error, reply)
      }
    }

    if (result == null)
      return

    if (typeof(status) === 'boolean') {
      if (callbackSuccess != null) {
        try {
          const resultCallback = await callbackSuccess(result)
          if (status === true)
            return resultCallback
        } catch (error) {
          app.transfer.error(error, reply)
        }
      }
      return
    }

    try {
      return app.transfer.success(reply, status, callbackSuccess ? await callbackSuccess(result) : result)
    } catch (error) {
      app.transfer.error(error, reply)
    }
  }


  app.decorate('performer', performer)
})
