/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
 #  Created by Saleh Rahimzadeh                                                                    #
 #  Copyright (C) 2020                                                                             #
 #  https://saleh-rahimzadeh.now.sh                                                                #
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
*/

'use strict'

// Framework
import MessagingFramework  from 'messaging-framework'
// Application
import Provider            from './app/provider.mjs'
// Messenger
import Messenger           from './app/messenger.mjs'
// Subscribers
import AuthSignUpSubscriber   from './subscribers/auth-signup.mjs'
import AuthWelcomeSubscriber  from './subscribers/auth-welcome.mjs'
import AuthSignInSubscriber   from './subscribers/auth-signin.mjs'
import BlogCreatedSubscriber  from './subscribers/blog-created.mjs'


MessagingFramework(
  Provider,
  [
    AuthSignUpSubscriber,
    AuthWelcomeSubscriber,
    AuthSignInSubscriber,
    BlogCreatedSubscriber
  ],
  Messenger
)
