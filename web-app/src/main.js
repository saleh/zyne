// Vue Modules
import { createApp } from 'vue'
// Application Modules
import router  from './router'
import store   from './store'
import App     from './App.vue'
// Third-Party Modules
import axios       from 'axios'
import HttpStatus  from 'http-status'
import dotenv      from 'dotenv'
import { Formatter }      from '@saleh-rahimzadeh/universal-nodejs/common.mjs'
import Text               from '@saleh-rahimzadeh/universal-vuejs/text.js'
// Internal Modules
import options      from './core/options.js'
import SetupRouter  from './router/setup.js'
import Bootstrap, { Setup, SetupPluginAxios, SetupPluginPrimseui }  from './setup.js'
// Plugin Modules
import EventServer  from './plugins/EventServer.js'
// PrimeVue Modules
import 'primevue/resources/themes/md-light-deeppurple/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'
import 'primeflex/primeflex.css'


const app = createApp(App)


Bootstrap(dotenv)

// Plugins
app.use(Text)
app.use(store)
app.use(router)
// Internal Plugins
app.use(EventServer, options.eventserver)

// Setup Router
SetupRouter(router, store)

// Setup
Setup(app, options, { axios, Formatter, HttpStatus })

// Setup Plugins
SetupPluginAxios(app, axios, options.gateway, store, router)

// PrimseUI Components
SetupPluginPrimseui(app)


const appRoot = app.mount('#app')
