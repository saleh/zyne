import BaseDatabase  from './base.mjs'
// Third-Party Modules
import MongoDB       from 'mongodb'


export default class DatabaseMongoDB extends BaseDatabase {

  constructor(app) {
    super(
      app,
      MongoDB.MongoClient,
      [
        app.daemons.database.connections.mongodb,
        {
          useUnifiedTopology: true,
        }
      ]
    )
    this.MongoDB = MongoDB
    this.collection = null
  }

  /**
   * Connect to database
   */
  async Connect() {
    await super.Connect()
    return this.client.db(this.app.organization.database.name)
  }

  /**
   * Get MongoDB collection
   */
  get Collection() {
    return this.collection
  }

  /**
   * Close database connection
   */
  async Close() {
    if (this.client.isConnected())
      await super.Close()
  }

  /**
   * Is id a valid ObjectID
   */
  isIdValid(id) {
    return this.MongoDB.ObjectID.isValid(id)
  }

  /**
   * Create an ObjectID
   * @param {string} id
   */
  createObjectID(id) {
    return { "_id": this.MongoDB.ObjectID(id) }
  }

}
