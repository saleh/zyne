// Internal Modules
import Handler  from '../../core/handler.mjs'
// Library Modules
import ServicesNameResolver  from 'libraries-collection/services-name-resolver.mjs'
// Third-Party Modules
import HttpStatus  from 'http-status'


export default class Handlers extends Handler {

  constructor(app) {
    super(app, ServicesNameResolver.COMMENT_SERVICE,
      [
        'Create',
        'Count',
        'Delete',
        'Like',
        'MostLiked'
      ],
      [
        'List'
      ]
    )
  }


  async List(request, reply) {
    const { blog_id, offset = 0, limit = 0 } = request.query
    return await this.app.performer(reply, this.service, 'List', { blog_id, offset, limit }, HttpStatus.OK)
  }


  async Create(request, reply) {
    const { blog_id, user_name, content } = request.body
    return await this.app.performer(reply, this.service, 'Create', { blog_id, user_name, content }, HttpStatus.CREATED)
  }


  async Delete(request, reply) {
    const { id } = request.params
    return await this.app.performer(reply, this.service, 'Delete', { id }, HttpStatus.NO_CONTENT)
  }


  async Count(request, reply) {
    const { blog_id } = request.query
    return await this.app.performer(reply, this.service, 'Count', { blog_id }, HttpStatus.OK)
  }


  async Like(request, reply) {
    const { id } = request.params
    return await this.app.performer(reply, this.service, 'Like', { id }, HttpStatus.CREATED)
  }


  async MostLiked(request, reply) {
    return await this.app.performer(reply, this.service, 'MostLiked', null, HttpStatus.OK)
  }

}
