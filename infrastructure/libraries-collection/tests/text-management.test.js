import text from '../text.js'

test("The 'Hi' must return 'Hello'", () => {
  expect(text('HI')).toBe("Hello")
})

test("The 'Fine' and argument 'Saleh' must return 'Hi how are you Saleh'", () => {
  expect(text('Fine', 'Saleh')).toBe("Hi how are you Saleh")
})
