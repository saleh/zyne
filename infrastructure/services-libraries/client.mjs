import grpc  from '@grpc/grpc-js'


export default function ServiceClient(serviceDescriptor, organization, serviceName) {
  const service = new serviceDescriptor[organization.descriptors[serviceName]](
    `localhost:${organization.services[serviceName]}`,
    grpc.credentials.createInsecure()
  )

  organization.endpoints[serviceName].forEach((item) => {
    service[item] = service[item].bind(service)
  })

  return service
}
