const primevue = {
  ripple: true
}


const gateway = {
  url: process.env.VUE_APP_GATEWAY_URL
}


const eventserver = {
  url: process.env.VUE_APP_EVENTSERVER_URL
}



export default {
  primevue,
  gateway,
  eventserver
}
