import app from 'services-framework/tests/instance.js'
import constants from '../../services-libraries/node_modules/@grpc/grpc-js/build/src/constants.js'

app.organization = {
  database: {
    name: "zyne_test",
    connections: {
      mongodb: 'mongodb://localhost:27017/?poolSize=5&w=majority&connectTimeoutMS=2500'
    }
  }
}

app.grpc = {
  status: constants.status
}

export default app
