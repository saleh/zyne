'use strict';


window.addEventListener('DOMContentLoaded', async () => {
  document.getElementById("btnCreate").addEventListener('click', createClicked)
})


async function createClicked() {
  const title = document.getElementById("title").value
  const content = document.getElementById("content").value
  await window.api.blogCreate(title, content)
  window.close()
}