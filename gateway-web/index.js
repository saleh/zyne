/*
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
 #  Created by Saleh Rahimzadeh                                                                    #
 #  Copyright (C) 2020                                                                             #
 #  https://saleh-rahimzadeh.now.sh                                                                #
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
*/

'use strict'

import application from './app/application.mjs'


let app
try {
  app = await application()
  await app.listen(
    app.organization.services['gateway-web'],
    '0.0.0.0'
  )
} catch (error) {
  /* eslint-disable no-console */
  console.error(error)
  /* eslint-enable no-console */

  process.exit(1)
}


/* eslint-disable no-console */
console.log("[ SERVER ]: %s", app.server.address())
console.log("[ ENVIRONMENT ]: %s", global.environment)
if (global.environment === global.environments.DEVELOPMENT)
  console.log("[ DOCUMENTATION ]: 0.0.0.0:%s/documentation", app.organization.services['gateway-web'])
/* eslint-enable no-console */


function shutdown() {
  if (!shutdown.isCalled)
    shutdown.isCalled = true
  else
    return

  app.close()
}

process.on('exit', shutdown)
process.on('SIGINT', shutdown)
process.on('SIGTERM', shutdown)
