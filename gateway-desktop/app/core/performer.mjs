export default function performer(app) {
  return async function performer(service, endpoint, data, callbackSuccess, callbackError) {
    let result

    try {
      result = await service[endpoint](data)
    } catch (error) {
      callbackError && await callbackError()
      app.exception(error)
    }

    if (result == null)
        return

    try {
      return callbackSuccess ? await callbackSuccess(result) : result
    } catch (error) {
      app.exception(error)
    }
  }
}