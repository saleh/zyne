export default function RouterSetup(router, store) {

  router.beforeEach((to, from) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
      if (!store.getters.isAuthenticated) {
        document.location = '/home'
        return false
      }
    }
    if (to.matched.some((record) => record.meta.requiresUnknown)) {
      if (store.getters.isAuthenticated)
        return { path: '/' }
    }
    return true
  })

}
