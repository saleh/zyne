import app  from './instance.js'
import Database  from '../app/database.js'

const database = new Database(app)


beforeAll(async () => {
  await database.Connect()
  await database.client.query({
    text: 'DELETE FROM comments'
  })
})

afterAll(async () => {
  await database.Close()
})


test("Insert a comment", async () => {
  const result = await database.Create({ blog_id: '123456789012345678901234', user_name: 'testUser1', content: 'Comment1' })
  expect(result).not.toBeNull()
  expect(result.rowCount).toBe(1)
  expect(result.data).toEqual(expect.objectContaining({
    id: expect.any(Number)
  }))
})

test("Count comments to be 1", async () => {
  const result = await database.Count({ blog_id: '123456789012345678901234' })
  expect(result).toBe(1)
})

test("List comments to be 1", async () => {
  const result = await database.List({ blog_id: '123456789012345678901234', skip: 0, limit: 'ALL' })
  expect(result.rowCount).toBe(1)
  expect(result.comments.length).toBe(1)
})

test("Like a comment", async () => {
  const list = await database.List({ blog_id: '123456789012345678901234', skip: 0, limit: 'ALL' })
  const result = await database.Like({ id: list.comments[0].id })
  expect(result.rowCount).toBe(1)
  expect(result.data.likes).toBe(1)
})

test("Delete a comment", async () => {
  const list = await database.List({ blog_id: '123456789012345678901234', skip: 0, limit: 'ALL' })
  const result = await database.Delete({ id: list.comments[0].id })
  expect(result.rowCount).toBe(1)
})
