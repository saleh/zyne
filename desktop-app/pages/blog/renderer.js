const { contextBridge, ipcRenderer } = require('electron')


contextBridge.exposeInMainWorld('api', {
  blogCreate: (title, content) => ipcRenderer.invoke('blog:create', title, content)
})
