import fs    from 'fs'
import path  from 'path'



const ResourceFiles = {
  AUTHENTICATION    : path.join(process.env.npm_package_constitution_resources,    'authentication.json'),
  SECRETKEY_PRIVATE : path.join(process.env.npm_package_constitution_certificates, 'authentication_private.pem'),
  SECRETKEY_PUBLIC  : path.join(process.env.npm_package_constitution_certificates, 'authentication_public.pem')
}


;(function checkFileExists() {
  Object.keys(ResourceFiles).forEach((key) => {
    if (!fs.existsSync(ResourceFiles[key])) {
      /* eslint-disable no-console */
      console.error(`AUTHENTICATION : Authentication File (${ResourceFiles[key]}) Not Found.`)
      /* eslint-enable no-console */
      process.exit(1)
    }
  })
})()


export default function Authentication() {
  return JSON.parse(fs.readFileSync(ResourceFiles.AUTHENTICATION, 'utf8'))
}

export function SecretKeyPublic() {
  return fs.readFileSync(ResourceFiles.SECRETKEY_PUBLIC, 'utf8')
}

export function SecretKeyPrivate() {
  return fs.readFileSync(ResourceFiles.SECRETKEY_PRIVATE, 'utf8')
}
