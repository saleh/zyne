// Library Modules
import AuthConfig, { SecretKeyPublic, SecretKeyPrivate }  from 'libraries-collection/authentication.mjs'



/*
fastify
*/

const fastifyOptions = {
  caseSensitive: false,
  ignoreTrailingSlash: true
}

if (global.environment !== global.environments.TEST) {
  fastifyOptions.logger = {
    level: 'info',
    name: 'Zyne-Gateway',
    prettyPrint: true
  }
}



/*
fastify-env
Schema and Options
*/

const envSchema = {
  type: 'object',
  required: [ 'AUTH_JWT_PASSPHRASE' ],
  properties: {
    AUTH_JWT_PASSPHRASE: { type: 'string' }
  }
}

const envOptions = {
  schema: envSchema,
  dotenv: global.environment === global.environments.PRODUCTION ? true : {
    path: '.env.development'
  }
}



/*
fastify-cors
*/

const corsOptions = {
  origin: true,
  credentials: true
}



/*
fastify-response-validation
*/

const responseValidationSchema = {
  responseValidation: global.environment === global.environments.DEVELOPMENT
}



/*
fastify-cookie
*/

const cookiePluginOptions = {
	secret: "zyne_ck@2021"
}

const cookieAuthenticationOptions = {
  httpOnly : true,
  sameSite : global.environment === global.environments.PRODUCTION,
  path     : '/',
  expires  : null,
  secure   : global.environment === global.environments.PRODUCTION
}

const cookieAuthorization = {
  name: 'authorization',
  options: {
    httpOnly : false,
    sameSite : global.environment === global.environments.PRODUCTION,
    path     : '/',
    expires  : null,
    secure   : global.environment === global.environments.PRODUCTION
  }
}



/*
fastify-jwt
*/

const jwtOptions = () => {
  return {
    secret: {
      private: {
        key: SecretKeyPrivate(),
        passphrase: process.env.AUTH_JWT_PASSPHRASE
      },
      public: SecretKeyPublic()
    },
    ...AuthConfig().jsonwebtoken
  }
}



/*
fastify-swagger
*/

const swaggerOptions = {
  routePrefix: "/documentation",
  swagger: {
    info: {
      title: "Zyne API",
      description: "Zyne API Documentation",
      version: '0.1.0'
    },
    host: 'localhost:5000',
    schemes: ['http'],
    tags: [
      { name: 'auth', description: "Auth related end-points" },
      { name: 'blog', description: "Blog related end-points" },
      { name: 'comment', description: "Comment related end-points" }
    ]
  },
  exposeRoute: true
}



/*
fastify-helmet
*/

const helmetOptions = {
  contentSecurityPolicy: global.environment === global.environments.PRODUCTION
}



/*
Module
*/
export default {
  fastifyOptions,
  envOptions,
  corsOptions,
  responseValidationSchema,
  cookieAuthenticationOptions,
  cookieAuthorization,
  cookiePluginOptions,
  jwtOptions,
  swaggerOptions,
  helmetOptions
}
