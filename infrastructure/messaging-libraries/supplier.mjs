// Native Modules
import { randomBytes }  from 'crypto'
// Third-Party Modules
import NATSStreaming    from 'node-nats-streaming'


export default function MessagingSupplier(app) {
  const clientID = `${global.name}_` + randomBytes(4).toString('hex')

  app.log.info("Messaging [%s] Connecting...", clientID)

  const NATS = NATSStreaming.connect(
    app.organization.messaging.cluster,
    clientID,
    {
      url: app.daemons.messaging.connection
    }
  )

  NATS.clientID = clientID

  return new Promise((resolve, reject) => {
    NATS.on('connect', () => {
      app.log.info("Messaging [%s] Connected", clientID)
      resolve(NATS)
    })

    NATS.on('error', (error) => {
      reject(error)
    })

    NATS.on('permission_error', (error) => {
      reject(error)
    })
  })
}

export function MessagingCloser(NATS, app) {
  NATS.close()
  return new Promise((resolve) => {
    NATS.on('close', () => {
      app.log.info("Messaging [%s] Closed", NATS.clientID)
      resolve()
    })
  })
}
