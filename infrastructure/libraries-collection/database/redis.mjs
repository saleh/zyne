import BaseDatabase  from './base.mjs'
// Third-Party Modules
import { Tedis }  from 'tedis'


export default class DatabaseRedis extends BaseDatabase {

  constructor(app) {
    const [ host, port ] = app.daemons.database.connections.redis.split(':')

    super(
      app,
      Tedis,
      [
        {
          host,
          port
        }
      ]
    )
    this.Redis = Tedis
    this.client.connect = () => {}

    this.isConnected = new Promise((resolve, reject) => {
      this.client.on('connect', () => {
        resolve()
      })
      this.client.on('error', (error) => {
        reject(error)
      })
    })

    this.client.on('error', (error) => {
      throw error
    })
  }

  async Connect() {
    await this.isConnected
    super.Connect()
  }

  get OK() {
    return "OK"
  }

}
