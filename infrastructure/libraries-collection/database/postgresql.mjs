import BaseDatabase  from './base.mjs'
// Third-Party Modules
import PostgreSQL    from 'pg'


export default class DatabasePostgreSQL extends BaseDatabase {

  constructor(app) {
    super(
      app,
      PostgreSQL.Client,
      [
        {
          connectionString: `${app.daemons.database.connections.postgresql}/${app.organization.database.name}`
        }
      ]
    )
    this.PostgreSQL = PostgreSQL
    this.client.close = this.client.end
  }

}
