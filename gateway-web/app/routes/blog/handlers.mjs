// Internal Modules
import Handler  from '../../core/handler.mjs'
// Library Modules
import ServicesNameResolver  from 'libraries-collection/services-name-resolver.mjs'
// Third-Party Modules
import HttpStatus  from 'http-status'


export default class Handlers extends Handler {

  constructor(app) {
    super(app, ServicesNameResolver.BLOG_SERVICE, 
      [
        'Create',
        'Count',
        'Read',
        'Update',
        'Delete',
        'MostLiked'
      ],
      [
        'List'
      ]
    )
  }


  async List(request, reply) {
    const { skip = 0, limit = 0 } = request.query
    return await this.app.performer(reply, this.service, 'List', { skip, limit }, HttpStatus.OK, true)
  }


  async Create(request, reply) {
    const { title, content } = request.body
    return await this.app.performer(reply, this.service, 'Create', { title, content }, HttpStatus.CREATED)
  }


  async Read(request, reply) {
    const { id } = request.params
    return await this.app.performer(reply, this.service, 'Read', { id }, HttpStatus.OK, true)
  }


  async Update(request, reply) {
    const { id } = request.params
    const { title, content } = request.body
    return await this.app.performer(reply, this.service, 'Update', { id, title, content }, HttpStatus.NO_CONTENT, false, () => {
      return {
        result: this.app.text('SUCCESSFUL')
      }
    })
  }


  async Delete(request, reply) {
    const { id } = request.params
    return await this.app.performer(reply, this.service, 'Delete', { id }, HttpStatus.NO_CONTENT)
  }


  async Count(request, reply) {
    return await this.app.performer(reply, this.service, 'Count', null)
  }


  async MostLiked(request, reply) {
    return await this.app.performer(reply, this.service, 'MostLiked')
  }

}
