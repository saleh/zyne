// Internal Modules
import { ExpressionTypes }  from 'libraries-collection/regexes.mjs'
// Third-Party Modules
import Password       from '@saleh-rahimzadeh/universal-nodejs/password.mjs'
import { Formatter }  from '@saleh-rahimzadeh/universal-nodejs/common.mjs'


export const TOKEN_LENGTH = {
  email: 10,
  phone: 5
}

export const SIGNUP_SIGNATURE = {
  email: 'UP',
  phone: '2'
}

export const SIGNIN_SIGNATURE = {
  email: 'IN',
  phone: '1'
}


export function SignUpTokenGenerate(identityType) {
  switch (identityType) {
  case ExpressionTypes.email:
    return Formatter("{sig}{otp}",
      SIGNUP_SIGNATURE.email,
      Password.OneTimePassword(TOKEN_LENGTH.email, { digits: true, alphabets: true, ualphabets: true, specials: false })
    )
  case ExpressionTypes.phone:
    return Formatter("{sig}{otp}",
      SIGNUP_SIGNATURE.phone,
      Password.OneTimePassword(TOKEN_LENGTH.phone, { digits: true, alphabets: false, ualphabets: false, specials: false })
    )
  }
}

export function SignInTokenGenerate(identityType) {
  switch (identityType) {
  case ExpressionTypes.email:
    return Formatter("{sig}{otp}",
      SIGNIN_SIGNATURE.email,
      Password.OneTimePassword(TOKEN_LENGTH.email, { digits: true, alphabets: true, ualphabets: true, specials: false })
    )
  case ExpressionTypes.phone:
    return Formatter("{sig}{otp}",
      SIGNIN_SIGNATURE.phone,
      Password.OneTimePassword(TOKEN_LENGTH.phone, { digits: true, alphabets: false, ualphabets: false, specials: false })
    )
  }
}
