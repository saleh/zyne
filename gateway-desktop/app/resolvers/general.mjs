export default {

  healthcheck(parent, args, context, info) {
    return {
      status: '✓',
      timestamp: (new Date()).toISOString()
    }
  }

}