const tags = ['blog']


const list = {
  tags,
  querystring: {
    type: 'object',
    properties: {
      skip: { type: 'integer', nullable: true },
      limit: { type: 'integer', nullable: true }
    },
  },
  response: {
    200: {
      type: 'array',
      items: {
        type: 'object',
        required: ['id', 'title'],
        properties: {
          id: { type: 'string' },
          title: { type: 'string' }
        }
      }
    }
  }
}

const create = {
  tags,
  body: {
    type: 'object',
    required: ['title', 'content'],
    properties: {
      title: { type: 'string' },
      content: { type: 'string' }
    }
  },
  response: {
    201: {
      type: 'object',
      required: ['blog_id'],
      properties: {
        blog_id: { type: 'string' }
      }
    }
  }
}

const read = {
  tags,
  params: {
    type: 'object',
    required: ['id'],
    properties: {
      id: { type: 'string' }
    }
  },
  response: {
    200: {
      type: 'object',
      required: ['title', 'content'],
      properties: {
        title: { type: 'string' },
        content: { type: 'string' }
      }
    }
  }
}

const update = {
  tags,
  params: {
    type: 'object',
    required: ['id'],
    properties: {
      id: { type: 'string' }
    }
  },
  body: {
    type: 'object',
    required: ['title', 'content'],
    properties: {
      title: { type: 'string' },
      content: { type: 'string' }
    }
  },
  response: {
    204: {
      type: 'object',
      properties: {
        result: { type: 'string', nullable: true }
      }
    }
  }
}

const remove = {
  tags,
  params: {
    type: 'object',
    required: ['id'],
    properties: {
      id: { type: 'string' }
    }
  },
  response: {
    204: { }
  }
}

const count = {
  tags,
  response: {
    200: {
      type: 'object',
      required: ['count'],
      properties: {
        count: { type: 'integer' }
      }
    }
  }
}

const mostliked = {
  tags,
  response: {
    200: {
      type: 'object',
      required: ['blog_id', 'blog_title', 'blog_content', 'comment_id', 'comment_insert_date', 'comment_user_name', 'comment_content', 'comment_likes'],
      properties: {
        blog_id: { type: 'string' },
        blog_title: { type: 'string' },
        blog_content: { type: 'string' },
        comment_id: { type: 'integer' },
        comment_insert_date: { type: 'string' },
        comment_user_name: { type: 'string' },
        comment_content: { type: 'string' },
        comment_likes: { type: 'integer' }
      }
    }
  }
}


export default {
  list,
  create,
  read,
  update,
  remove,
  count,
  mostliked
}
