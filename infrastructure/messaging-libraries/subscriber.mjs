const ACK_WAIT = 5 * 1000  // 5 seconds


export default class Subscriber {

  constructor(app, messaging, provider) {
    this.app = app
    this.messaging = messaging
    this.provider = provider
    this.subscription = null
    this.channel = null
  }

  load(channel, { durable = false, acknowledgeable = false } = {}) {
    this.app.log.info("\t\t [%s] Subscribing...", channel)

    if (channel == null)
      throw new Error("Undefined 'onLoad()' Method on Subscriber")

    this.channel = channel

    const opts = this.messaging.subscriptionOptions()

    if (durable) {
      opts.setDurableName(`${global.name}_durable`)
      opts.setDeliverAllAvailable()
    } else
      opts.setStartTime(Date.now())

    if (acknowledgeable) {
      opts.setManualAckMode(true)
      opts.setAckWait(ACK_WAIT)
      opts.setMaxInFlight(1)
    }

    this.subscription = this.messaging.subscribe(this.channel, `${global.name}_queue`, opts)

    if (!Object.prototype.hasOwnProperty.call(this.constructor.prototype, 'onMessage'))
      throw new Error("Undefined 'onMessage(msg, data)' Method On Subscriber")
    this.subscription.on('message', (msg) => {
      this.onMessage(msg, this.parseMessage(msg.getData()))
    })

    if (Object.prototype.hasOwnProperty.call(this.constructor.prototype, 'onReady'))
      this.subscription.on('ready', () => { this.onReady() })

    if (Object.prototype.hasOwnProperty.call(this.constructor.prototype, 'onUnsubscribed'))
      this.subscription.on('unsubscribed', () => { this.onUnsubscribed() })

    if (Object.prototype.hasOwnProperty.call(this.constructor.prototype, 'onError'))
      this.subscription.on('error', () => { this.onError() })

    this.subscription.on('error', (error) => {
      this.app.log.error(error)
    })
  }

  async subscribe() {
    return new Promise((resolve) => {
      this.subscription.on('ready', () => {
        this.app.log.info("\t\t [%s] Subscribed", this.channel)
        resolve()
      })
    })
  }

  unsubscribe() {
    this.subscription.unsubscribe()
    return new Promise((resolve) => {
      this.subscription.on('unsubscribed', () => {
        this.app.log.info("\t\t [%s] Unsubscribed", this.channel)
        resolve()
      })
    })
  }

  parseMessage(data) {
    if (data === "" || data == null)
      return null
    try {
      return JSON.parse(data)
    } catch (error) {
      this.app.log.error("Invalid Message Data")
      return null
    }
  }

}
