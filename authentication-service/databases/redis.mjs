import DatabaseRedis  from 'libraries-collection/database/redis.mjs'


export default class Database extends DatabaseRedis {

  async CreateOtp(key, expire, identity, data) {
    const result = await this.client.hmset(key, {
      identity,
      ...data
    })

    if (result !== this.OK)
      return false

    await this.client.expire(key, expire)

    return true
  }


  async IsOtpExists(key) {
    return await this.client.exists(key) === 1 ? true : false
  }


  async FindOtp(key, fields) {
    const result = await this.client.hmget(key, "identity", ...fields)
    const [ identity, ...data ] = result
    return {
      identity,
      data
    }
  }


  async DeleteOtp(key) {
    return await this.client.del(key) > 0 ? true : false
  }


  async GetAllOtp(key) {
    return await this.client.keys(key)
  }

  async GetOtpTTL(key) {
    return await this.client.ttl(key)
  }

  async RemoveOtp(key) {
    await this.client.del(...key)
  }

}
