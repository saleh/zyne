// Internal Modules
import Handler  from '../../core/handler.mjs'
// Library Modules
import ServicesNameResolver  from 'libraries-collection/services-name-resolver.mjs'
// Third-Party Modules
import HttpStatus  from 'http-status'


export default class Handlers extends Handler {

  constructor(app) {
    super(app, ServicesNameResolver.AUTHENTICATION_SERVICE,
      [
        'SignUpInvite',
        'SignUp',
        'SignInInvite',
        'SignIn',
        'SignOut'
      ]
    )
  }


  async SignUpInvite(request, reply) {
    const { identity, name } = request.body
    return await this.app.performer(reply, this.service, 'SignUpInvite', { identity, name }, HttpStatus.OK)
  }

  async SignUp(request, reply) {
    const { token } = request.params
    return await this.app.performer(reply, this.service, 'SignUp', { token }, HttpStatus.CREATED, false, async (result) => {
      try {
        await this.app.authentication.Grant(reply, result.user_id, result.issued_at)
        return {
          user_id: result.user_id
        }
      } catch (error) {
        return error
      }
    })
  }

  async SignInInvite(request, reply) {
    const { identity } = request.body
    return await this.app.performer(reply, this.service, 'SignInInvite', { identity }, HttpStatus.OK)
  }

  async SignIn(request, reply) {
    const { token } = request.params
    return await this.app.performer(reply, this.service, 'SignIn', { token }, HttpStatus.OK, false, async (result) => {
      try {
        await this.app.authentication.Grant(reply, result.user_id, result.issued_at)
        return {
          user_id: result.user_id
        }
      } catch (error) {
        return error
      }
    })
  }

  async SignOut(request, reply) {
    let user_id
    if (this.app.authentication.IsAuthenticated(request))
      user_id = request.user.sub

    this.app.authentication.Deny(request, reply)

    if (user_id == null) {
      reply.status(HttpStatus.NO_CONTENT)
      return {}
    }

    return await this.app.performer(reply, this.service, 'SignOut', { user_id }, HttpStatus.NO_CONTENT)
  }

}
