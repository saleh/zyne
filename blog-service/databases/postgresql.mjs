import DatabasePostgreSQL  from 'libraries-collection/database/postgresql.mjs'


export default class Database extends DatabasePostgreSQL {

  async MostLiked() {
    const result = await this.client.query({
      text: "SELECT * FROM comments WHERE likes = (SELECT MAX(likes) AS max FROM comments)"
    })

    const consequence = {
      rowCount: result.rowCount,
      data: null
    }
    if (result.rowCount)
      consequence.data = result.rows[0]

    return consequence
  }

}
