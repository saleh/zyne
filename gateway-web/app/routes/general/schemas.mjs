const healthcheck = {
  tags: ['Healthcheck'],
  description: 'Healthcheck endpoint to determine if service is up and running',
  response: {
    200: {
      type: 'object',
      properties: {
        status: { type: 'string' },
        timestamp: { type: 'string', format: 'date-time' }
      }
    }
  }
}


export default {
  healthcheck
}
