// Library Modules
import ServiceBinder  from 'libraries-collection/service-binder.mjs'
// Third-Party Modules
import { Binder }  from '@saleh-rahimzadeh/universal-nodejs/common.mjs'


export default class Resolver {

  constructor(app, serviceName, endpoints, endpointsStreaming) {
    Binder(this)
    
    this.service = new ServiceBinder(
      app.service.services, 
      serviceName,
      endpoints, 
      endpointsStreaming
    )
  }

}