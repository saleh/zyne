import BaseSubscriber  from 'messaging-libraries/subscriber.mjs'


export default class BlogCreatedSubscriber extends BaseSubscriber {

  onLoad() {
    super.load(
      this.app.organization.channels[global.name].blog.created,
      {
        durable: false,
        acknowledgeable: false
      }
    )
  }


  onMessage(msg, data) {
    this.app.log.trace(`[${msg.getSequence()}]`, msg.getSubject())
    this.provider.blog_created(data)
  }

}
