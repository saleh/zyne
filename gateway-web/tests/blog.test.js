'use strict'

// Application
import uri          from '../app/core/uri.js'
import application  from '../app/application.js'


let global = {}
let app

beforeAll(async () => {
  app = await application()
})

afterAll(async () => {
  app.close()
})


// test("Create a blog", async () => {
//     const response = await supertestApp
//       .post(uri.API_BLOG)
//       .send({
//         title: 'blogTest1',
//         content: 'Blog Test 1'
//       })
//       .expect(201)
//       .expect('Content-Type', 'application/json; charset=utf-8')
//     console.log(response)
//     expect(response).not.toBeNull()
//     const result = response.body
//     expect(result).toEqual(expect.objectContaining({
//       blogId: expect.any(String)
//     }))
//     expect(result.blogId.length).toBe(24)
// })

test("List blogs", async () => {
  const response = await app.inject({
    method: 'GET',
    url: uri.API_BLOG
  })
  expect(response).not.toBeNull()
  expect(response.statusCode).toEqual(200)
  const result = JSON.parse(response.body)
  expect(Array.isArray(result)).toBeTruthy()
})

// test("Read a blog", async () => {
//     const response = await app.inject({
//       method: 'GET',
//       url: `${uri.paths.API_BLOG}/5f8710d02a77c64767c708af`,
//     })
//     console.log(response)
//     expect(response).not.toBeNull()
//     expect(response.statusCode).toEqual(200)
//     const result = JSON.parse(response.body)
//     expect(result).toEqual(expect.objectContaining({
//       title: expect.any(String),
//       content: expect.any(String)
//     }))
//     expect(result.title).toMatch(/blog3/)
// })

// test("Count blogs", async () => {
//   const response = await app.inject({
//     method: 'GET',
//     url: `${uri.API_BLOG}/count`
//   })
//   expect(response).not.toBeNull()
//   expect(response.statusCode).toEqual(200)
//   const result = JSON.parse(response.body)
//   expect(result).toEqual(expect.objectContaining({
//     count: expect.any(Number)
//   }))
//   expect(result.count).toBeGreaterThan(0)
// })
