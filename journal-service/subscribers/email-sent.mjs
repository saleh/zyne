import BaseSubscriber  from 'messaging-libraries/subscriber.mjs'


export default class EmailSentSubscriber extends BaseSubscriber {

  onLoad() {
    super.load(
      this.app.organization.channels[global.name].email.sent,
      {
        durable: true,
        acknowledgeable: true
      }
    )
  }


  onMessage(msg, data) {
    this.app.log.trace(`[${msg.getSequence()}]`, msg.getSubject())
    this.provider.email_sent(data)
    msg.ack()
  }

}
