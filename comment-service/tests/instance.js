import app from 'services-framework/tests/instance.js'

app.organization = {
  database: {
    name: "zyne_test",
    connections: {
      postgresql: 'postgresql://postgres:postgres@localhost:5432'
    }
  }
}

export default app
