import Button     from 'primevue/button'
import InputText  from 'primevue/inputtext'
import Message    from 'primevue/message'
import Textarea   from 'primevue/textarea';



export default function Bootstrap(dotenv) {
  dotenv.config()
}


export function Setup(app, options, { axios, Formatter, HttpStatus }) {
  app.config.globalProperties.$Http = axios
  app.$Http = axios

  app.config.globalProperties.$Formatter = Formatter
  app.$Formatter = Formatter

  app.config.globalProperties.$HttpStatus = HttpStatus
  app.$HttpStatus = HttpStatus

  app.config.globalProperties.$primevue = options.primevue
}


export function SetupPluginAxios(app, axios, options, store, router) {
  axios.defaults.withCredentials = true
  axios.defaults.baseURL = options.url

  axios.interceptors.response.use(undefined, function(error) {
    if (error) {
      if (error.response.status === app.$HttpStatus.UNAUTHORIZED) {
        store.commit("removeUser")
        document.location = '/home'
      }
      throw error
    }
  })
}


export function SetupPluginPrimseui(app) {
  app.component('Button', Button);
  app.component('InputText', InputText)
  app.component('Message', Message)
  app.component('Textarea', Textarea)
}
