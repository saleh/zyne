export default {
  HOME      : '/',
  COUNT     : '/count',
  MOSTLIKED : '/mostliked',
  params : {
    ID : '/:id'
  }
}
