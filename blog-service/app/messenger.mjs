import BaseMessenger         from 'services-framework/messenger/base.mjs'
import ServicesNameResolver  from 'libraries-collection/services-name-resolver.mjs'


export default class Messenger extends BaseMessenger {

  async emailBlogCreated(data) {
    try {
      await this.publish(this.channels[ServicesNameResolver.EMAIL_SERVICE].blog.created, data)
    } catch (error) {
      this.app.log.error(error)
    }
  }

  async journalBlogDeleted(data) {
    try {
      await this.publish(this.channels[ServicesNameResolver.JOURNAL_SERVICE].blog.deleted, data)
    } catch (error) {
      this.app.log.error(error)
    }
  }

}
