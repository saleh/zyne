const ServicesNameResolver = {
  EMAIL_SERVICE   : 'email-service',
  JOURNAL_SERVICE : 'journal-service',
  EVENT_SERVICE: 'event-service',
  AUTHENTICATION_SERVICE: 'authentication-service',
  BLOG_SERVICE: 'blog-service',
  COMMENT_SERVICE: 'comment-service'
}

export default ServicesNameResolver