#!/bin/bash

SERVER_PORT=8100

echo "SERVER: Port=" $SERVER_PORT

docker run                                             \
  --name zyne_pages                                    \
  --publish $SERVER_PORT:80                            \
  --network zyne_network                               \
  --volume $(pwd)/pages:/pages:ro                      \
  --volume $(pwd)/assets:/assets:ro                    \
  --volume $(pwd)/nginx.conf:/etc/nginx/nginx.conf:ro  \
  --rm                                                 \
  nginx:alpine
