export default class BaseDatabase {

  constructor(app, client, options) {
    this.app = app

    const prototypeName = Object.getPrototypeOf(this.constructor).name
    if (!Object.prototype.isPrototypeOf.call(BaseDatabase, this.constructor) || !prototypeName.startsWith('Database'))
      throw new Error('Not Database Class')
    this.name = prototypeName.replace('Database', '')

    this.app.log.info("\t\t [%s] Connecting...", this.name)

    this.client = new client(...options)
  }

  /**
   * Connect to database
   */
  async Connect() {
    await this.client.connect()
    this.app.log.info("\t\t [%s] Connected", this.name)
  }

  /**
   * Close database connection
   */
  async Close() {
    await this.client.close()
    this.app.log.info("\t\t [%s] Closed", this.name)
  }

}
