import ms  from 'ms'


export default class Sessions {

  static async Create(callback, user_id) {
    const issued_at = new Date()

    let result
    try {
      result = await this.database.mongodb.CreateSession(user_id, issued_at)
      if (result.modifiedCount === 0 && result.upsertedCount === 0)
        return this.exception(null, callback, this.status.INTERNAL, 'ERROR_CREATE_SESSION')
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    return {
      issued_at,
      result
    }
  }


  static async DeleteByUserId(user_id) {
    try {
      await this.database.mongodb.DeleteSessionByUserId(user_id)
    } catch (error) {
      return error
    }
  }


  static async FindByUserId(callback, user_id) {
    let result
    try {
      result = await this.database.mongodb.FindSessionByUserId(user_id)
      if (result == null)
        return this.exception(null, callback, this.status.UNAUTHENTICATED, 'ERROR_USER_UNAUTHENTICATED')
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    return result
  }


  static async DeleteExpired(durationTimestamp) {
    try {
      this.database.mongodb.DeleteExpiredSessions(new Date(Date.now() - ms(durationTimestamp)))
    } catch (error) {
      this.app.log.error(error)
    }
  }


  static async UpdateIssueDate(callback, session_id) {
    const issued_at = new Date()

    let result
    try {
      result = await this.database.mongodb.UpdateSessionIssueDate(session_id, issued_at)
      if (result == null)
        return this.exception(null, callback, this.status.UNAUTHENTICATED, 'ERROR_USER_UNAUTHENTICATED')
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    return {
      issued_at,
      result
    }
  }

}
