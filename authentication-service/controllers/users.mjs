import { ExpressionTypes }  from 'libraries-collection/regexes.mjs'


export default class Users {

  static async IsExist(callback, user_id) {
    try {
      if (await this.database.mongodb.FindUser(user_id, ['_id'] != null))
        return true
      else
        return this.exception(null, callback, this.status.NOT_FOUND, 'ERROR_USER_NOT_FOUND')
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }
  }


  static async Create(callback, name, email, phone) {
    let result
    try {
      result = await this.database.mongodb.CreateUser(name, email, phone)
      if (result.insertedCount === 0)
        return this.exception(null, callback, this.status.INTERNAL, 'ERROR_CREATE_USER')
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    return result
  }


  static async CheckExists(callback, identityType, identity) {
    let result

    try {
      switch (identityType) {
      case ExpressionTypes.email:
        result = await this.database.mongodb.FindUserByEmail(identity)
        if (result == null)
          return this.exception(null, callback, this.status.NOT_FOUND, 'ERROR_USER_NOT_FOUND')
        break
      case ExpressionTypes.phone:
        result = await this.database.mongodb.FindUserByPhone(identity)
        if (result == null)
          return this.exception(null, callback, this.status.NOT_FOUND, 'ERROR_USER_NOT_FOUND')
        break
      }
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    return result
  }


  static async CheckNotExists(callback, identityType, identity) {
    try {
      switch (identityType) {
      case ExpressionTypes.email:
        if (await this.database.mongodb.FindUserByEmail(identity) != null) {
          this.exception(null, callback, this.status.ALREADY_EXISTS, 'ERROR_USER_EXISTS')
          return true
        }
        break
      case ExpressionTypes.phone:
        if (await this.database.mongodb.FindUserByPhone(identity) != null) {
          this.exception(null, callback, this.status.ALREADY_EXISTS, 'ERROR_USER_EXISTS')
          return true
        }
        break
      }
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    return false
  }


  static async Find(callback, user_id) {
    let result
    try {
      result = await this.database.mongodb.FindUser(user_id, ['enabled'])
      if (result == null)
        return this.exception(null, callback, this.status.NOT_FOUND, 'ERROR_USER_NOT_FOUND')
    } catch (error) {
      return this.exception(error, callback, this.status.INTERNAL)
    }

    return result
  }

}
