/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
 #  Created by Saleh Rahimzadeh                                                                    #
 #  Copyright (C) 2020                                                                             #
 #  https://saleh-rahimzadeh.now.sh                                                                #
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
*/

'use strict'

// Framework
import ServicesFramework   from 'services-framework'
// Application
import Application         from './app/application.mjs'
import Messenger           from './app/messenger.mjs'
// Databases
import DatabaseMongoDB     from './databases/mongodb.mjs'
import DatabasePostgreSQL  from './databases/postgresql.mjs'
import DatabaseRedis       from './databases/redis.mjs'


ServicesFramework(
  Application,
  {
    Database: [ DatabaseMongoDB, DatabasePostgreSQL, DatabaseRedis ],
    Messenger
  },
  {
    Memorize: true
  }
)
