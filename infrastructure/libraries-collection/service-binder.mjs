// Buildin Modules
import { promisify }  from 'util'


/**
 * Service Binder
 *
 * Binding service endpoints
 *
 * @param {Object} servicesContainer - services
 * @param {String} name - name of service
 * @param {Array<String>} endpoints - endpoints, must be promisify
 * @param {Array<String>} endpointsStreaming - endpoints, haven't be promisify
 */
export default class ServiceBinder {

  constructor(servicesContainer, name, endpoints, endpointsStreaming) {
    this.name = name

    const services = servicesContainer[this.name]

    if (endpoints != null) {
      endpoints.forEach((item) => {
        this[item] = promisify(services[item])
      })
    }
    if (endpointsStreaming != null) {
      endpointsStreaming.forEach((item) => {
        this[item] = ServiceBinder.Stream(services[item])
      })
    }
  }

  static Stream(endpoint) {
    return async function (listRequest) {
      const caller = endpoint(listRequest)
      const listReply = []

      return new Promise((resolve, reject) => {
        caller.on('data', (item) => {
          listReply.push({ ...item })
        })

        caller.on('end', () => {
          resolve(listReply)
        })

        caller.on("error", (error) => {
          reject(error.details)
        })
      })
    }
  }

}
