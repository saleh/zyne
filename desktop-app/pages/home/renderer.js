'use strict';

const { contextBridge, ipcRenderer } = require('electron')


contextBridge.exposeInMainWorld('api', {
  
})


const api = {
  blogList: () => ipcRenderer.invoke('blog:list'),
  blogRead: (blogId) => ipcRenderer.invoke('blog:read', blogId),
  commentList: (blogId) => ipcRenderer.invoke('comment:list', blogId),
  commentLike: (commentId) => ipcRenderer.invoke('comment:like', commentId),
  commentDelete: (commentId) => ipcRenderer.invoke('comment:delete', commentId),
  commentCreate: (blogId, username, content) => ipcRenderer.invoke('comment:create', blogId, username, content),
  dialogCommentCreated: (id) => ipcRenderer.send('dialog:commentcreated', id),
  blogNew: () => ipcRenderer.send('blog:new')
}



window.addEventListener('DOMContentLoaded', async () => {
  await loadBlogsList()
  document.getElementById("linkNewBlog").addEventListener('click', newBlogClicked)
  document.getElementById("btnNewComment").addEventListener('click', newCommentClicked)
})


async function loadBlogsList() {
  const blogList = await api.blogList()

  const blogElements = blogList.map(item => createBlogItem(item))

  if (blogElements.length > 0) {
    document.getElementById('blog-list').append(...blogElements)
    selectFirstBlog(blogList[0].id, blogElements[0])
  } else {
    document.getElementById('blog-holder').style.display = "none"
  }
}

function createBlogItem(item) {
  const blog = document.createElement('blog-item')
  blog.setAttribute('template', 'blog-item-template')
  blog.dataset.id = item.id
  blog.innerHTML = item.title
  blog.addEventListener('selected', blogSelected)
  return blog
}

async function blogSelected(event) {
  document.getElementById('blog-holder').dataset.id = event.detail.id
  const blog = await api.blogRead(event.detail.id)
  document.getElementById("blog-title").innerHTML = blog.title
  document.getElementById("blog-text").innerHTML = blog.content
  await loadCommentsList(event.detail.id)
}

function selectFirstBlog(id, blog) {
  const itemClickedEvent = new CustomEvent('selected', {
    detail: {
      id
    }
  })
  blog.dispatchEvent(itemClickedEvent)
}

async function loadCommentsList(blogId) {
  const commentList = await api.commentList(blogId)

  const commentElements = commentList.map(item => createCommentItem(item))
  const commentListElement = document.getElementById('comment-list')
  while (commentListElement.firstChild)
      commentListElement.removeChild(commentListElement.lastChild)
  if (commentElements.length > 0)
    commentListElement.append(...commentElements)
}

function createCommentItem(item) {
  const comment = document.createElement('comment-item')
  comment.setAttribute('template', 'comment-item-template')
  comment.dataset.id = item.id
  comment.dataset.insertdate = new Date(item.insertDate).toLocaleString()
  comment.dataset.username = item.userName
  comment.setAttribute('likes', item.likes)
  comment.innerHTML = item.content
  comment.addEventListener('liked', commentLiked)
  comment.addEventListener('removed', commentRemoved)
  return comment
}

async function commentLiked(event) {
  const commentLikes = await api.commentLike(event.detail.id)
  event.target.setAttribute('likes', commentLikes.likes)
}

async function commentRemoved(event) {
  const commentDelete = await api.commentDelete(event.detail.id)
  if (commentDelete === true)
    event.target.remove()
}

async function newCommentClicked() {
  const blogid = document.getElementById('blog-holder').dataset.id
  const username = document.getElementById('comment-username').value
  const content = document.getElementById('comment-content').value
  const commentCreate = await api.commentCreate(blogid, username, content)
  await api.dialogCommentCreated(commentCreate.commentId)
  loadCommentsList(blogid)
  document.getElementById('comment-username').value = ""
  document.getElementById('comment-content').value = ""
}


ipcRenderer.on('event:blog:added', (event, id, title) => {
  const blog = createBlogItem({ id, title })
  document.getElementById('blog-list').appendChild(blog)
  const itemClickedEvent = new CustomEvent('selected', {
    detail: {
      id
    }
  })
  blog.dispatchEvent(itemClickedEvent)
})


function newBlogClicked() {
  api.blogNew()
}