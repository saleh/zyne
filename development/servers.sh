#!/bin/bash

case $1 in
  "MONGODB"|"mongodb")
    MONGODB
    ;;
  "POSTGRESQL"|"postgresql")
    POSTGRESQL
    ;;
  "NATSSTREAMING"|"natsstreaming")
    NATSSTREAMING
    ;;
  "REDIS"|"redis")
    REDIS
    ;;
  "NGINX"|"nginx")
    NGINX
    ;;
  "down")
    docker-compose --file docker-compose.servers.yml down
    ;;
  "up")
    docker-compose --file docker-compose.servers.yml up
    ;;
  *)
    ;;
esac



function MONGODB {
  docker run                               \
    --name zyne_mongodb                    \
    --publish 27017:27017                  \
    --network zyne_network                 \
    --volume zyne_mongodb_volume:/data/db  \
    --rm                                   \
    mongo
}

function POSTGRESQL {
  docker run                                                \
    --name zyne_postgresql                                  \
    --publish 5432:5432                                     \
    --network zyne_network                                  \
    --volume zyne_postgres_volume:/var/lib/postgresql/data  \
    --env POSTGRES_PASSWORD=postgres                        \
    --env POSTGRES_USER=postgres                            \
    --rm                                                    \
    postgres:alpine
}

function REDIS {
  docker run                          \
    --name zyne_redis                 \
    --publish 6379:6379               \
    --network zyne_network            \
    --volume zyne_redis_volume:/data  \
    --rm                              \
    redis:alpine
}

function NATSSTREAMING {
  docker run                   \
    --name zyne_natsstreaming  \
    --publish 4222:4222        \
    --publish 8222:8222        \
    --network zyne_network     \
    --rm                       \
    nats-streaming             \
      --cluster_id zyne_cluster
}

function NGINX {
  docker run                                                                             \
    --name zyne_nginx                                                                    \
    --publish 80:80                                                                      \
    --network zyne_network                                                               \
    --volume "$(dirname "$(pwd)")/deployment/nginx-proxy.conf":/etc/nginx/nginx.conf:ro  \
    --rm                                                                                 \
    nginx:alpine
}
