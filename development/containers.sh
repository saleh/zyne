#!/bin/bash

case $1 in
  "up")
    docker-compose up
    ;;
  "build")
    docker-compose build
    ;;
  "run")
    docker run --name $2 --publish $3 --network zyne_network --rm $2
    ;;
  "down")
    docker-compose down
    ;;
  *)
    docker-compose up --no-deps
    ;;
esac