// Library Modules
import DatabaseRedis  from 'libraries-collection/database/redis.mjs'
// Third-Party Modules
import fastifyPlugin  from 'fastify-plugin'
import { Tedis }      from "tedis"


class Redis extends DatabaseRedis {
}


export default fastifyPlugin(async (app) => {
  
  const serviceApp = {
    log: app.log,
    organization: app.organization,
    daemons: app.daemons
  }

  app.log.info('Databases :')

  let redis
  try {
    redis = new Redis(serviceApp)
  } catch (error) {
    app.transfer.error(error)
    return
  }

  await redis.Connect()

  async function close() {
    app.log.info("Databases :")
    await redis.Close()
  }


  app.decorate('database', {
    redis: redis,
    close
  })
})
