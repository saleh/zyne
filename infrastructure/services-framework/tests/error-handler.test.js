import ErrorHandler from '../app/error-handler.js'
import app from './instance.js'

const errorHandler = ErrorHandler(app)

/*
const app = {
  log: {
    info: jest.fn(msg => msg),
    error: jest.fn(errObj => {
      return {
        error: errObj.error,
        message: errObj.message,
        details: errObj.details
      }
    })
  },
  text: jest.fn((msg, ...places) => msg)
}

const err1 = {
  error: 1,
  message: "1 Error: Test error 1",
  details: "Test error 1"
}
*/

test("Test 1", () => {
  let data

  const mockCallback = (errObj) => {
    data = {
      code: errObj.code,
      message: errObj.message
    }
  }

  errorHandler(null, mockCallback, 1, 'test1')

  expect(data).toEqual(expect.objectContaining({
     code: 1,
     message: 'test1'
  }))
})

test("Test 2", () => {
  let data

  const mockCallback = (errObj) => {
    data = {
      code: errObj.code,
      message: errObj.message
    }
  }

  errorHandler(null, mockCallback, 2, 'test2', 'a', 'b')

  expect(data).toEqual(expect.objectContaining({
     code: 2,
     message: 'test2 a b'
  }))
})
