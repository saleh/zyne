import S from 'fluent-json-schema'

const tags = ['comment']


const list = {
  tags,
  querystring: S.object()
    .prop('blog_id', S.string().required())
    .prop('offset', S.integer())
    .prop('limit', S.integer()),
  response: {
    200: {
      type: 'array',
      items: {
        type: 'object',
        required: ['id', 'insert_date', 'user_name', 'content', 'likes'],
        properties: {
          id: { type: 'integer' },
          insert_date: { type: 'string' },
          user_name: { type: 'string' },
          content: { type: 'string' },
          likes: { type: 'integer' }
        }
      }
    }
  }
}

const create = {
  tags,
  body: S.object()
    .prop('blog_id', S.string().required())
    .prop('user_name', S.string().required())
    .prop('content', S.string().required()),
  response: {
    201: {
      type: 'object',
      required: ['comment_id'],
      properties: {
        comment_id: { type: 'string' }
      }
    }
  }
}

const remove = {
  tags,
  params: S.object()
    .prop('id', S.integer().required()),
  response: {
    204: { }
  }
}

const count = {
  tags,
  querystring: S.object()
    .prop('blog_id', S.string().required()),
  response: {
    200: {
      type: 'object',
      required: ['count'],
      properties: {
        count: { type: 'integer' }
      }
    }
  }
}

const like = {
  tags,
  params:  S.object()
    .prop('id', S.integer().required()),
  response: {
    200: {
      type: 'object',
      required: ['likes'],
      properties: {
        likes: { type: 'integer' }
      }
    }
  }
}

const mostliked = {
  tags,
  response: {
    200: {
      type: 'object',
      required: ['blog_id', 'blog_title', 'blog_content', 'comment_id', 'comment_insert_date', 'comment_user_name', 'comment_content', 'comment_likes'],
      properties: {
        blog_id: { type: 'string' },
        blog_title: { type: 'string' },
        blog_content: { type: 'string' },
        comment_id: { type: 'integer' },
        comment_insert_date: { type: 'string' },
        comment_user_name: { type: 'string' },
        comment_content: { type: 'string' },
        comment_likes: { type: 'integer' }
      }
    }
  }
}


export default {
  list,
  create,
  remove,
  count,
  like,
  mostliked
}
