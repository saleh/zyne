import BaseProvider  from 'messaging-framework/provider/base.mjs'
// Buildin  Modules
import HttpServer  from 'http'
// Internal Modules
import config  from './configs.mjs'
// Third-Party Modules
import { Binder, Random, Formatter }  from '@saleh-rahimzadeh/universal-nodejs/common.mjs'
import Cookie      from 'cookie'
import HttpStatus  from 'http-status'
import JWT         from 'jsonwebtoken'


const FIELDS = {
  EVENT : "event: {1}\n",
  DATA  : "data: {1}\n",
  ID    : "id: {1}\n",
  RETRY : "retry: {1}\n"
}

const NEW_LINE = "\n"


export default class Provider extends BaseProvider {

  constructor(app) {
    super(app)

    this.clients = new Map()

    Binder(this)

    this.app.log.info("Event Server Starting...")

    this.http = HttpServer.createServer(this.requestListener)
    this.http.listen(this.app.organization.services[global.name])

    this.app.log.info("Event Server [%s]", this.app.organization.services[global.name])
  }

  async OnClose() {
    this.app.log.info("Closing Event Server...")

    this.clients.forEach(response => {
      response.end()
    })

    this.http.close()

    if (this.clients.size === 0)
      return Promise.resolve()

    return new Promise((resolve) => {
      setTimeout(() => {
        if (this.clients.size === 0)
          resolve()
      }, 1000)
    })
  }

  requestListener(request, response) {
    // Set response header
    response.setHeader(config.RESPONSE_CORS_HEADERS.Origin, request.headers.origin)
    response.setHeader(config.RESPONSE_CORS_HEADERS.Credentials, "true")
    Object.keys(config.RESPONSE_HEADERS).forEach((key) => {
      response.setHeader(key, config.RESPONSE_HEADERS[key])
    })

    // Reject if non Event-Stream
    if (!Object.prototype.hasOwnProperty.call(request, 'headers') ||
        !Object.prototype.hasOwnProperty.call(request.headers, 'accept') ||
        request.headers.accept !== 'text/event-stream')
      return this.rejectWithForbidden(response)

    // Check authenticated
    if (!this.verifyAuth(request))
      return this.rejectWithForbidden(response)

    if (request.url === '/') {  // Accept `/` url
      // Set event response headers
      Object.keys(config.EVENT_HEADERS).forEach((key) => {
        response.setHeader(key, config.EVENT_HEADERS[key])
      })
      // Handle request
      this.requestHandler(request, response)
    } else {  // Unkonw route
      response.writeHead(HttpStatus.NOT_FOUND)
      response.end()
    }
  }

  rejectWithForbidden(response) {
    response.writeHead(HttpStatus.FORBIDDEN)
    response.end()
  }

  verifyAuth(request) {
    let token

    // Extract token from http header
    if (Object.prototype.hasOwnProperty.call(request.headers, 'authorization')) {
      const parts = request.headers.authorization.split(' ')
      if (parts.length === 2) {
        const [ rawToken, scheme ] = parts
        if (!/^Bearer$/i.test(scheme))
          return false
        else
          token = rawToken
      } else
        return null
    } else if (Object.prototype.hasOwnProperty.call(request.headers, 'cookie')) {
      const cookies = Cookie.parse(request.headers.cookie)
      token = cookies[config.jwtOptions.cookie.cookieName]
      if (token == null)
        return false
    } else
      return false

    // Verify token
    try {
      JWT.verify(token, config.jwtOptions.secret, config.jwtOptions.verify)
      return true
    } catch (error) {
      return false
    }
  }

  requestHandler(request, response) {
    const clientID = Formatter("{date}-{random}", Date.now(), Random(9999, 1000))

    this.app.log.info("Connection [%s]", clientID)

    response.writeHead(HttpStatus.OK)
    response.write(NEW_LINE)

    this.clients.set(clientID, response)

    request.on('close', () => {
      this.app.log.info("Disconnection [%s]", clientID)
      this.clients.delete(clientID)
    })
  }

  serializeEvent(fieldId, fieldEvent, fieldData, fieldRetry) {
    let payload = ""

    if (fieldId != null)
      payload += Formatter(FIELDS.ID, fieldId)
    if (fieldEvent != null)
      payload += Formatter(FIELDS.EVENT, fieldEvent)
    if (fieldData != null)
      payload += Formatter(FIELDS.DATA, fieldData)
    if (fieldRetry != null)
      payload += Formatter(FIELDS.RETRY, fieldRetry)

    return payload !== "" ?
      payload + NEW_LINE :
      ""
  }

  reply(name, data) {
    this.clients.forEach(response => {
      response.write(
        this.serializeEvent(
          (new Date()).toISOString(),
          name,
          JSON.stringify(data)
        )
      )
    })
  }


  blog_created(data) {
    this.app.log.trace("Blog Created: %s", data)
    this.reply("blog_created", data)
  }

}
