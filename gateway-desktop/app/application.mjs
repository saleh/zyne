// Internal
import "libraries-collection/environment.mjs"
// Apollo-Server
import { ApolloServer } from 'apollo-server'
// Third-Party Modules
import log4js        from 'log4js'
import dotenv        from 'dotenv'
// Application
import app           from './core/instance.mjs'
// Library Modules
import Organization    from 'libraries-collection/organization.mjs'
import Text            from 'libraries-collection/text.mjs'
import Daemons         from 'libraries-collection/daemons.mjs'
// Service Modules
import ServiceSupplier  from 'services-libraries/supplier.mjs'
// Core Modules
import config     from "./core/config.mjs"
import exception  from "./core/exception.mjs"
import performer  from "./core/performer.mjs"
// Application Modules
import typeDefs   from "./type-definitions.mjs"
import resolvers, { resolverDefs }  from "./resolvers.mjs"


export default async () => {

  // --- [ PLUGINS ] -------------------------------------------------------------------------------

  dotenv.config()

  app.log = log4js.getLogger()
  app.log.level = 'ALL'

  // --- [ LIBRARIES ] -----------------------------------------------------------------------------

  app.log.info("[GATEWAY-DESKTOP] Starting...")

  app.organization = Organization()
  app.text = Text()
  app.daemons = Daemons()

  // --- [ CORE ] ---------------------------------------------------------------------------------

  app.exception = exception(app)
  app.performer = performer(app)

  // --- [ SERVICES ] ------------------------------------------------------------------------------

  app.service = ServiceSupplier({
    log: app.log,
    organization: app.organization
  })

  // --- [ RESOLVERS ] -----------------------------------------------------------------------------

  resolvers(app)

  // --- [ SERVER ] --------------------------------------------------------------------------------

  const server = new ApolloServer({
    typeDefs,
    resolvers: resolverDefs(),
    context: () => ({
      ...app
    })
  })

  return {
    server,
    ...app
  }

}
