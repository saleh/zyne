// Buildin Libraries
import fs    from 'fs'
import path  from 'path'
// GRPC Libraries
import grpc    from '@grpc/grpc-js'
import loader  from '@grpc/proto-loader'


function checkFileExists(PROTOFILE) {
  if (!fs.existsSync(PROTOFILE)) {
    /* eslint-disable no-console */
    console.error(`DESCRIPTOR : Service Descriptor File (${PROTOFILE}) Not Found.`)
    /* eslint-enable no-console */
    process.exit(1)
  }
}


/**
 * Service Descriptor Definition
 * @param {String} protobufFile - protobuf name and path of file
 */
function ServiceDescriptorDefinition(protobufFile) {
  checkFileExists(protobufFile)

  const serviceDescriptor = grpc.loadPackageDefinition(
    loader.loadSync(
      protobufFile,
      {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
      }
    )
  )

  return serviceDescriptor
}


/**
 * Create Service Descriptor
 * @param {String} name - protobuf name of file
 * @param {JSON} protobuf - protobuf data
 */
export default function ServiceDescriptor(name, protobuf) {
  const PROTOFILE = path.join(
    protobuf.path,
    protobuf.files[name]
  )

  return ServiceDescriptorDefinition(PROTOFILE)
}

/**
 * Create Health Service Descriptor
 * @param {JSON} protobuf - protobuf data
 */
export function HealthServiceDescriptor(protobuf) {
  const PROTOFILE = path.join(
    protobuf.path,
    "health.proto"
  )

  return ServiceDescriptorDefinition(PROTOFILE)
}
