// Third-Party
import grpc  from '@grpc/grpc-js'
// Library
import ServiceDescriptor, { HealthServiceDescriptor }  from 'services-libraries/descriptor.mjs'


export default function Server(app, provider) {
  app.log.info("Server Connecting [%s]...", app.organization.services[global.name])

  const endpoints = {}
  for (const point of app.organization.endpoints[global.name])
    endpoints[point] = provider[point].bind(provider)

  const server = new grpc.Server()

  const serviceDescriptor = ServiceDescriptor(global.name, app.organization.protobuf)

  server.addService(
    serviceDescriptor[app.organization.descriptors[global.name]].service,
    endpoints
  )

  const healthServiceDescriptor = HealthServiceDescriptor(app.organization.protobuf)

  server.addService(
    healthServiceDescriptor.Health.service,
    {
      Check: provider.HealthCheck.bind(provider)
    }
  )

  return new Promise((resolve, reject) => {
    server.bindAsync(
      `0.0.0.0:${app.organization.services[global.name]}`,
      grpc.ServerCredentials.createInsecure(),
      (error, boundPort) => {
        if (error != null)
          reject(error)
        server.start()
        app.log.info("Server Bound : %s", boundPort)
        resolve(server)
      }
    )
  })
}
