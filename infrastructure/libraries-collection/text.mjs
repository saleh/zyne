import fs          from 'fs'
import process     from 'process'
// Third-Party Modules
import { Formatter }  from '@saleh-rahimzadeh/universal-nodejs/common.mjs'


const strings = new Map()

const TEXT_FILE = 'strings.text'


function checkFileExists() {
  if (!fs.existsSync(TEXT_FILE)) {
    /* eslint-disable no-console */
    console.error(`TEXT : Text File (${TEXT_FILE}) Not Found.`)
    /* eslint-enable no-console */
    process.exit(1)
  }
}

function loadTexts(separator) {
  fs
    .readFileSync(TEXT_FILE, 'utf8')
    .split(/\n|\r|\r\n/gm)
    .forEach(line => {
      if (!line)
        return

      const indexOfKey = line.indexOf(separator)
      if (indexOfKey < 1)
        throw new Error("Error in parsing strings")

      strings.set(
        line.slice(0, indexOfKey).trim(),
        line.slice(indexOfKey + 1).trimStart()
      )
    })
}

function Texter(name, ...places) {
  if (!strings.has(name))
    return name
  return Formatter(strings.get(name), ...places)
}


export default function Text({ separator = '=' } = {}) {
  if (typeof(separator) !== 'string')
    throw new Error("The separator parameter must be string type")
  if (/^[:=|;,.?@#%&*]$/.test(separator) === false)
    throw new Error("The separator parameter must be 1 character (:=|;.,?@#%&*) delimiter")

  checkFileExists()
  loadTexts(separator)

  return Texter
}
