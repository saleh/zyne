export default {
  log: {
    info: () => {},
    error: () => {},
    trace: () => {}
  },
  text: (msg, ...places) => msg + (places.length > 0 ? ' ' + places.join(' ') : ''),
  organization: null
}
