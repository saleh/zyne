// Third-Party
import fastifyPlugin  from 'fastify-plugin'
import HttpStatus     from 'http-status'
import { Contains }   from '@saleh-rahimzadeh/universal-nodejs/common.mjs'


export default fastifyPlugin(async (app) => {

  function errorHandler(error, reply) {
    if (error instanceof Error !== true) {
      app.log.error(error)
      throw app.httpErrors.internalServerError()
    }

    if (Contains(error, ['code', 'details'])) {
      switch (error.code) {
      case 5:   // NOT_FOUND
        throw app.httpErrors.notFound(error.details)
      case 3:   // INVALID_ARGUMENT
        throw app.httpErrors.badRequest(error.details)
      case 4:   // DEADLINE_EXCEEDED
        throw app.httpErrors.badRequest(error.details)
      case 6:   // ALREADY_EXISTS
        throw app.httpErrors.badRequest(error.details)
      case 7:   // PERMISSION_DENIED
        throw app.httpErrors.forbidden(error.details)
      case 16:  // UNAUTHENTICATED
        throw app.httpErrors.unauthorized(error.details)
      case 13:  // INTERNAL
        throw app.httpErrors.internalServerError(error.details)
      default:  // Other
        throw app.httpErrors.internalServerError(error.details)
      }
    }

    app.log.error(error)
    throw app.httpErrors.internalServerError()
  }


  /**
   * Success Handler
   * @param {Reply} reply - Reply object
   * @param {Number|Null} status - Status Number
   * @param {Result|Null} result - Result object
   */
  function successHandler(reply, status, result = {}) {
    if (Number.isInteger(status)) {
      if (result != null && result instanceof Error)
        throw result
      reply.status(status)
    } else {
      reply.status(HttpStatus.OK)
    }
    return result
  }


  app.decorate('transfer', {
    error: errorHandler,
    success: successHandler
  })
})
