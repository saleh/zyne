import uri       from './uri.mjs'
import schemas   from './schemas.mjs'
import Handlers  from './handlers.mjs'


export default async function blog(app) {
  const handlers = new Handlers(app)


  /**
   * SignUp Invite
   * @route POST /auth/signup
   */
  app.post(
    uri.SIGNUP,
    {
      schema: schemas.signup_invite,
      preValidation: app.authentication.RejectIfAuthenticated
    },
    handlers.SignUpInvite
  )

  /**
   * SignUp
   * @route GET /auth/signup/:token
   */
  app.get(
    `${uri.SIGNUP}${uri.params.TOKEN}`,
    {
      schema: schemas.signup,
      preValidation: app.authentication.RejectIfAuthenticated
    },
    handlers.SignUp
  )

  /**
   * SignIn Invite
   * @route POST /auth/signin
   */
  app.post(
    uri.SIGNIN,
    {
      schema: schemas.signin_invite,
      preValidation: app.authentication.RejectIfAuthenticated
    },
    handlers.SignInInvite
  )

  /**
   * SignIn
   * @route GET /auth/signin/:token
   */
  app.get(
    `${uri.SIGNIN}${uri.params.TOKEN}`,
    {
      schema: schemas.signin,
      preValidation: app.authentication.RejectIfAuthenticated
    },
    handlers.SignIn
  )

  /**
   * SignOut
   * @route GET /auth/signout
   */
  app.get(
    uri.SIGNOUT,
    {
      schema: schemas.signout
    },
    handlers.SignOut
  )

}
