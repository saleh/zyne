/*
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
 #  Created by Saleh Rahimzadeh                                                                    #
 #  Copyright (C) 2020                                                                             #
 #  https://saleh-rahimzadeh.now.sh                                                                #
 #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
*/

'use strict'

import application from './app/application.mjs'


let app
try {
  app = await application()
  const { server } = await app.server.listen({
    port: app.organization.services['gateway-desktop'],
    host: '0.0.0.0'
  })

  /* eslint-disable no-console */
  console.log("[ SERVER ]: %s", server.address())
  console.log("[ ENVIRONMENT ]: %s", global.environment)
  if (global.environment === global.environments.DEVELOPMENT)
    console.log("[ DOCUMENTATION ]: 0.0.0.0:%s", app.organization.services['gateway-desktop'])
  /* eslint-enable no-console */

} catch (error) {
  /* eslint-disable no-console */
  console.error(error)
  /* eslint-enable no-console */

  process.exit(1)
}


function shutdown() {
  if (!shutdown.isCalled)
    shutdown.isCalled = true
  else
    return

  app.log.info('Shutdowning...')
  app.service.close()
  app.log.info('Shutdowned')

  //process.exit()
}

function terminate(error) {
  app.log.error(error)
  process.exitCode = 1
  shutdown()
}

process.on('exit', shutdown)
process.on('SIGINT', shutdown)
process.on('SIGTERM', shutdown)
process.on('unhandledRejection', (reason) => { throw reason })
process.on('uncaughtException', (error) => terminate(error))