// Internal Modules
import Resolver  from '../core/resolver.mjs'
// Library Modules
import ServicesNameResolver  from 'libraries-collection/services-name-resolver.mjs'


export default class Blog extends Resolver {

  constructor(app) {
    super(app, ServicesNameResolver.BLOG_SERVICE, 
      [
        'Create',
        'Count',
        'Read',
        'Update',
        'Delete',
        'MostLiked'
      ],
      [
        'List'
      ]
    )
  }


  async Count(parent, args, context, info) {
    return await context.performer(this.service, 'Count', null, (result) => result.count)
  }

  async List(parent, args, context, info) {
    const { skip = 0, limit = 0 } = args.request || { skip : 0, limit : 0 }
    return await context.performer(this.service, 'List', { skip, limit })
  }

  async Create(parent, args, context, info) {
    const { title, content } = args.blog
    return await context.performer(this.service, 'Create', { title, content }, (result) => ({ blogId: result.blog_id }))
  }

  async Read(parent, args, context, info) {
    const id = args.id
    return await context.performer(this.service, 'Read', { id })
  }

  async MostLiked(parent, args, context, info) {
    return await context.performer(this.service, 'MostLiked', null, (result) => ({
      blogId: result.blog_id,
      blogTitle: result.blog_title,
      blogContent: result.blog_content,
      commentId: result.comment_id,
      commentInsertDate: result.comment_insert_date,
      commentUserName: result.comment_user_name,
      commentContent: result.comment_content,
      commentLikes: result.comment_likes
    }))
  }

  async Update(parent, args, context, info) {
    const id = args.id
    const { title, content } = args.blog
    return await context.performer(this.service, 'Update', { id, title, content }, () => true)
  }

  async Delete(parent, args, context, info) {
    const id = args.id
    return await context.performer(this.service, 'Delete', { id }, () => true)
  }

}