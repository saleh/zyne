use zyne;

db.dropDatabase();


db.createCollection('blogs', {
  validator: {
    $jsonSchema: {
      bsonType: 'object',
      required: ['title', 'content'],
      properties: {
        title: { bsonType: 'string' },
        content: { bsonType: 'string' }
      }
    }
  }
});

db.createCollection('users', {
  validator: {
    $jsonSchema: {
      bsonType: 'object',
      properties: {
        email: { bsonType: 'string' },
        phone: { bsonType: 'string' },
        name: { bsonType: 'string' },
        enabled: { bsonType: 'bool' }
      }
    }
  }
});

db.createCollection('sessions', {
  validator: {
    $jsonSchema: {
      bsonType: 'object',
      properties: {
        user_id: { bsonType: 'objectId' },
        created_at: { bsonType: 'date' },
        issued_at: { bsonType: 'date' }
      }
    }
  }
});
