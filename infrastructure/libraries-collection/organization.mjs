// Buildin Modules
import fs    from 'fs'
import path  from 'path'
// Third-Party Modules
import yaml  from 'js-yaml'


const ORGFILE = path.join(
  process.env.npm_package_constitution_resources,
  'organization.yml'
)


function checkFileExists() {
  if (!fs.existsSync(ORGFILE)) {
    /* eslint-disable no-console */
    console.error(`ORGANIZATION : Organization File (${ORGFILE}) Not Found.`)
    /* eslint-enable no-console */
    process.exit(1)
  }
}


export default function Organization() {
  checkFileExists()
  return yaml.safeLoad(fs.readFileSync(ORGFILE, 'utf8'))
}
