/**
 * Regex Expressions
 */
export const Expressions = {
  email: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
  phone: /^09\d{9}$/
}

export const ExpressionTypes = {
  email: 1,
  phone: 2
}

export default class RegExChecker {

  static whatIsThis(identity) {
    if (RegExChecker.isEmail(identity))
      return ExpressionTypes.email
    if (RegExChecker.isPhone(identity))
      return ExpressionTypes.phone
    throw new TypeError()
  }

  static isEmail(identity) {
    return Expressions.email.test(identity)
  }

  static isPhone(identity) {
    return Expressions.phone.test(identity)
  }

}
