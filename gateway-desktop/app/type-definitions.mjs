import { gql } from 'apollo-server'


export default gql`

  type HealthCheck {
    status: String!
    timestamp: String!
  }

  # BLOG
  # -----------------------------------------------------------------------------------------------

  input BlogListInput {
    skip: Int
    limit: Int
  }

  type BlogList {
    id: String!
    title: String!
  }

  type BlogRead {
    title: String!
    content: String!
  }

  input BlogUpdateInput {
    title: String!
    content: String!
  }

  input BlogCreateInput {
    title: String!
    content: String!
  }

  type BlogCreate {
    blogId: String!
  }
  
  type BlogMostLiked {
    blogId: String!
    blogTitle: String!
    blogContent: String!
    commentId: Int!
    commentInsertDate: String!
    commentUserName: String!
    commentContent: String!
    commentLikes: String!
  }

  # COMMENT
  # -----------------------------------------------------------------------------------------------

  input CommentListInput {
    blogId: String!
    offset: Int
    limit: Int
  }

  type CommentList {
    id: Int!
    insertDate: String!
    userName: String!
    content: String!
    likes: Int!
  }

  input CommentCreateInput {
    blogId: String!
    userName: String!
    content: String!
  }

  type CommentCreate {
    commentId: String!
  }

  type CommentLike {
    likes: Int!
  }

  type CommentMostLiked {
    blogId: String!
    blogTitle: String!
    blogContent: String!
    commentId: Int!
    commentInsertDate: String!
    commentUserName: String!
    commentContent: String!
    commentLikes: String!
  }



  # QUERY
  # -----------------------------------------------------------------------------------------------

  type Query {
    healthcheck: HealthCheck!
    blog_list(request: BlogListInput): [BlogList!]
    blog_count: Int!
    blog_read(id: String!): BlogRead!
    blog_mostliked: BlogMostLiked!
    comment_count(blogId: String!): Int!
    comment_list(request: CommentListInput!): [CommentList!]
    comment_mostliked: CommentMostLiked!
  }



  # MUTATION
  # -----------------------------------------------------------------------------------------------

  type Mutation {
    blog_create(blog: BlogCreateInput!): BlogCreate!
    blog_update(id: String!, blog: BlogUpdateInput!): Boolean
    blog_delete(id: String!): Boolean
    comment_create(comment: CommentCreateInput!): CommentCreate!
    comment_delete(id: Int!): Boolean!
    comment_like(id: Int!): CommentLike!
  }

`