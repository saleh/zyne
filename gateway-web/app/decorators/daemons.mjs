import Daemons  from 'libraries-collection/daemons.mjs'


export default (app) => {
  app.decorate(
    'daemons',
    Daemons()
  )
}
