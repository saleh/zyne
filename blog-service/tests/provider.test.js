import app       from './instance.js'
import Database  from '../app/database-mongodb.js'
import Provider  from '../app/provider.js'


const database = {
  mongodb: new Database(app)
}
const provider = new Provider(app, () => {}, database)


let data = null

function callback(error, result) {
  data.error = error
  data.result = result
}


beforeAll(async () => {
  await database.mongodb.Connect()
  await database.mongodb.Collection.deleteMany({})
})

afterAll(async () => {
  await database.mongodb.Collection.drop()
  await database.mongodb.Close()
})

beforeEach(() => {
  data = {
    error: null,
    result: null
  }
})


test("Insert a blog", async () => {
  const request = { request: { title: 'blog1', content: 'Blog 1' }}
  await provider.Create(request, callback)
  expect(data.error).toBeNull()
  expect(data.result).toEqual(expect.objectContaining({
    blog_id: expect.any(Object)
  }))
  expect(database.mongodb.isIdValid(data.result.blog_id)).toBeTruthy()
})

test("Count blogs to be 1", async () => {
  await provider.Count(null, callback)
  expect(data.error).toBeNull()
  expect(data.result).toEqual(expect.objectContaining({
    count: expect.any(Number)
  }))
  expect(data.result.count).toBe(1)
})

test("List blogs to be 1", async () => {
  const blogArray = []
  const request = {
    request: { skip: 0, limit: 0 },
    write: (itm) => { blogArray.push(itm) },
    end: () => {},
    destroy: (error) => { callback(error) }
  }
  await provider.List(request, callback)
  expect(data.error).toBeNull()
  expect(blogArray.length).toBe(1)
})

test("Read a blog", async () => {
  const blogArray = []
  const requestList = {
    request: { skip: 0, limit: 0 },
    write: (itm) => { blogArray.push(itm) },
    end: () => {},
    destroy: (error) => { callback(error) }
  }
  await provider.List(requestList)
  const requestRead = { request: { id: blogArray[0].id }}
  await provider.Read(requestRead, callback)
  expect(data.error).toBeNull()
  expect(data.result).toEqual(expect.objectContaining({
    title: expect.any(String),
    content: expect.any(String)
  }))
  expect(data.result.title).toMatch(/blog1/)
  expect(data.result.content).toMatch(/Blog/)
})

test("Update a blog", async () => {
  const blogArray = []
  const requestList = {
    request: { skip: 0, limit: 0 },
    write: (itm) => { blogArray.push(itm) },
    end: () => {},
    destroy: (error) => { callback(error) }
  }
  await provider.List(requestList)
  const requestUpdate = { request: { id: blogArray[0].id, title: 'blog11', content: 'Blog 11' }}
  await provider.Update(requestUpdate, callback)
  expect(data.error).toBeNull()
  expect(data.result).toBeNull()
})

test("Delete a blog", async () => {
  const blogArray = []
  const requestList = {
    request: { skip: 0, limit: 0 },
    write: (itm) => { blogArray.push(itm) },
    end: () => {},
    destroy: (error) => { callback(error) }
  }
  await provider.List(requestList)
  const requestDelete = { request: { id: blogArray[0].id }}
  await provider.Delete(requestDelete, callback)
  expect(data.error).toBeNull()
  expect(data.result).toBeNull()
})
