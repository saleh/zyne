import { createRouter, createWebHashHistory, createWebHistory } from 'vue-router'
import Home      from '../views/Home.vue'
import SignUp    from '../views/SignUp.vue'
import SignIn    from '../views/SignIn.vue'
import Blog      from '../views/Blog.vue'
import Error404  from '../views/404.vue'


const routes = [
  {
    path: "/",
    name: 'Home',
    component: Home,
    meta: { requiresAuth: true }
  },
  {
    path: "/signup/:token",
    name: 'SignUp',
    component: SignUp,
    meta: { requiresUnknown: true }
  },
  {
    path: "/signin/:token",
    name: 'SignIn',
    component: SignIn,
    meta: { requiresUnknown: true }
  },
  {
    path: "/blog/:blog_id",
    name: 'Blog',
    component: Blog,
    meta: { requiresAuth: true }
  },
  {
    path: "/:pathMatch(.*)*",
    name: 'Error404',
    component: Error404
  }
]


const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
