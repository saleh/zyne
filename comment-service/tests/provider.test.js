import app       from './instance.js'
import Database  from '../app/database.js'
import Provider  from '../app/provider.js'
import constants from '../../services-framework/node_modules/@grpc/grpc-js/build/src/constants.js'

const database = new Database(app)
Provider.load(
  app,
  database,
  constants.status,
  () => {}
)

let data = null

function callback(error, result) {
  data.error = error
  data.result = result
}


beforeAll(async () => {
  await database.Connect()
  await database.client.query({
    text: 'DELETE FROM comments'
  })
})

afterAll(async () => {
  await database.Close()
})

beforeEach(() => {
  data = {
    error: null,
    result: null
  }
})


test("Insert a comment", async () => {
  const request = { request: { blog_id: '123456789012345678901234', user_name: 'testUser1', content: 'Comment1' }}
  await Provider.Create(request, callback)
  expect(data.error).toBeNull()
  expect(data.result).toEqual(expect.objectContaining({
    comment_id: expect.any(Number)
  }))
})

test("Count comments to be 1", async () => {
  const commentArray = []
  const request = {
    request: { blog_id: '123456789012345678901234' },
    write: (itm) => { commentArray.push(itm) },
    end: () => {},
    destroy: (error) => { callback(error) }
  }
  await Provider.Count(request, callback)
  expect(data.error).toBeNull()
  expect(data.result).toEqual(expect.objectContaining({
    count: expect.any(Number)
  }))
  expect(data.result.count).toBe(1)
})

test("List Comments to be 1", async () => {
  const commentArray = []
  const request = {
    request: { blog_id: '123456789012345678901234', skip: 0, limit: 'ALL' },
    write: (itm) => { commentArray.push(itm) },
    end: () => {},
    destroy: (error) => { callback(error) }
  }
  await Provider.List(request)
  expect(data.error).toBeNull()
  expect(commentArray.length).toBe(1)
})

test("Like a comment", async () => {
  const commentArray = []
  const request = {
    request: { blog_id: '123456789012345678901234', skip: 0, limit: 'ALL' },
    write: (itm) => { commentArray.push(itm) },
    end: () => {},
    destroy: (error) => { callback(error) }
  }
  await Provider.List(request)
  const requestLike = { request: { id: commentArray[0].id }}
  await Provider.Like(requestLike, callback)
  expect(data.error).toBeNull()
  expect(data.result).toEqual(expect.objectContaining({
    likes: expect.any(Number)
  }))
  expect(data.result.likes).toBe(1)
})

test("Delete a comment", async () => {
  const commentArray = []
  const request = {
    request: { blog_id: '123456789012345678901234', skip: 0, limit: 'ALL' },
    write: (itm) => { commentArray.push(itm) },
    end: () => {},
    destroy: (error) => { callback(error) }
  }
  await Provider.List(request)
  const requestDelete = { request: { id: commentArray[0].id }}
  await Provider.Delete(requestDelete, callback)
  expect(data.error).toBeNull()
  expect(data.result).toBeNull()
})
