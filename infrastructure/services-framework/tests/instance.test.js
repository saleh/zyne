import instance from '../app/instance.js'

test("Instance must has correct members", () => {
  expect(Object.keys(instance).length).toBe(3)
})

test("Instance members must be null", () => {
  expect(instance).toEqual(expect.objectContaining({
    log: null,
    text: null,
    organization: null
  }))
})
