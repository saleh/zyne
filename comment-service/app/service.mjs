// Library Modules
import ServicesNameResolver  from 'libraries-collection/services-name-resolver.mjs'
import ServiceBinder         from 'libraries-collection/service-binder.mjs'
// Third-Party Modules
import { Binder }  from '@saleh-rahimzadeh/universal-nodejs/common.mjs'


export default class Service {

  constructor(services) {
    Binder(this)

    this.endpoints = new ServiceBinder(services, ServicesNameResolver.BLOG_SERVICE,
      [
        'Read'
      ]
    )
  }


  /**
   * Read a blog
   * @param {object} ReadRequest
   * @returns {Promise<ReadReply>}
   */
  async ReadBlog(readRequest) {
    return await this.endpoints.Read(readRequest)
  }

}
